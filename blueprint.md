# **OpenModelica functions**
_pseudocode for the om-functions_ <br>
_self wont be described in parametersections_

#### global reminder:
  - check for `None` as result for functions that call OpenModelicas internal API

### **omOpen(self)**
  - **@params**: none
  - set up a OM server
    - ~~[ ] OMCSession~~
    - [x] ModelicaSystem
      - [has its own
      OMCSession](https://github.com/OpenModelica/OMPython/blob/master/OMPython/__init__.py#L491)
  - set options like `dymOpen()` does ?

### **omTranslate(self)**
  - **@params**: none
  - ~~should compile the model (to FMU?)~~
    - [`ModelicaSystem.convertMo2Fmu()`](https://github.com/OpenModelica/OMPython/blob/master/OMPython/__init__.py#L1467)
  - compile the model to c code?
    - Already done in [ModelicaSystem's constructor](https://github.com/OpenModelica/OMPython/blob/master/OMPython/__init__.py#L482)?
      - precisely in with [the call](https://github.com/OpenModelica/OMPython/blob/master/OMPython/__init__.py#L587) to its [internal API](https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/scripting_api.html#buildmodel)
  - check the result and catch exception

### **omApplyModifier(self, modifiersList)**
  - **@params**:
    - _modifiersList_: 
      - format: [['submodule1','param1','value1'],['submodule2','param2','value2'],...]
  - change the given parameters with the coresponding value
    - [x] [`ModelicaSystem.setParameters(param1=val1,...)`](https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/ompython.html#usage-of-setmethods) ?

### **omSimulate(self, simOptions = "", resultName = "simResults")**
  - **@params**:
    - _simOptions_:
      - simply add as parameter to
      [`ModelicaSystem.setSimulationOptions(_simOptions_)`](https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/ompython.html#usage-of-setmethods)?
      - are these options like eg. `startTime`?
    - _resultName_:
      - not needed as OpenModelica automatically writes the results to a file called _"modelName_res.mat"_
    - **TODO** check how to aply these options ([does such an option exist?](https://github.com/OpenModelica/OMPython/blob/master/OMPython/__init__.py#L513))
    - set options
    - call ModelicaSystem's [`simulate()`](https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/ompython.html#simulation) function

### **omSimulateMultiResults(self,  resultName = "simResult", simOptions = "", initialNames="", initialValues="", resultNames="")**
  - **@params**:
    - _resultName_:
      - string with name of the file, the result should be written to
    - _simOptions_:
      - ?
    - _initialNames_:
      - ?
    - _initialValues_:
      - ? 
    - _resultNames_:
      - not needed as OpenModelica automatically writes the results to a file called _"modelName_res.mat"_
  - **TODO** check which options are available
  - see omSimulate for further informations

### **omGetResultsFast(self, resultName = "simResult", outputVarList = [])**
  - **@params**:
    - _resultName_:
      - not needed as OpenModelica automatically writes the results to a file called _"modelName_res.mat"_
    - _outputVarList_:
      - array with the wanted variables
  - return an numpy.array containing the trajectories for the variables in outputVarList using ModelicaRes's [`SimRes()`](http://kdavies4.github.io/ModelicaRes/modelicares.simres.html) function

### **omGetResults(self, resultName = "simResult", outputVarList = [])** 
  - **@params**:
    - _resultName_:
      - not needed as OpenModelica automatically writes the results to a file called _"modelName_res.mat"_
    - _outputVarList_:
      - array with the wanted variables
  - values are read from the file with the name `resultName`
  - return numpy.array with the results for the given variables
  - OpenModelica's function [`getSolutions()`](https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/ompython.html#usage-of-getmethods)
    - check if the variablesNames are in it
    - get value with [`getSolutions(variableName1, variableName2,...)`](https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/ompython.html#usage-of-getmethods), where variableName is a string containing the wanted variables's name

### **omGetResultVarNames(self, resultName = "simResult")**
  - **@params**:
    - _resultName_:
      - not needed as OpenModelica automatically writes the results to a file called _"modelName_res.mat"_
  - return the variable names from the simulation result
  - OpenModelica's function [`getSolutions()`](https://openmodelica.org/doc/OpenModelicaUsersGuide/latest/ompython.html#usage-of-getmethods)

### **omClose(self)**
  - **@params**: none
  - ~~close the OpenModelica session with `ModelicaSystem.getconn.sendExpression(close())` like [here](https://github.com/OpenModelica/OMPython/blob/master/OMPython/__init__.py#L585)~~
  - set the ModelicaSystem-Object `self.__omObj` to `None`
    - ModelicaSystem handles shutdown of OMCSession etc. in its [Destructor](https://github.com/OpenModelica/OMPython/blob/master/OMPython/__init__.py#L200)

# **function overview**
_self wont be described in parametersections_
### **dymOpen(self)**
  - **@params**: none
  - opens a dymola sessions
  - loads the given model
  - sets some options
    
### **dymTranslate(self)**
  - **@params**: none
  - tries to use Dymola's `translateModel(self._model)` function (compiles the model)
  - checks for result and catches exception

### **dymApplyModifiers(self, modifiersList)**
  - **@params**:
    - _modifiersList_: 
      - format: [['submodule1','param1','value1'],['submodule2','param2','value2'],...]
  - creates string with instructions and sends it to `self.__dymObj.ExecuteCommand(string)` 
  - form of instruction: `subModuleN.paramN = valueN`

### **dymSimulate(self, simOptions = "", resultName = "simResults")**
  - **@params**:
    - _simOptions_:
      - ?
    - _resultName_:
      - string with name of the file, the result should be written to
  - calls Dymola's `simulateModel()` function with the given parameters

### **dymSimulateMultiResults(self,  resultName = "simResult", simOptions = "", initialNames="", initialValues="", resultNames="")**
  - **@params**:
    - _resultName_:
      - string with name of the file, the result should be written to
    - _simOptions_:
      - ?
    - _initialNames_:
      - ?
    - _initialValues_:
      - ? 
    - _resultNames_:
      - ?
  - calls Dymola's `simulateMultiResults()` like `dymSimulate()` does, but gives some extra parameters 

### **dymGetResultsFast(self, resultName = "simResult", outputVarList = [])**
  - **@params**:
    - _resultName_:
      - string with name of the file, the result is written to
    - _outputVarList_:
      - array with the wanted variables
  - returns an numpy.array containing the trajectories for the variables in outputVarList using ModelicaRes's `SimRes()` function

### **dymGetResults(self, resultName = "simResult", outputVarList = [])** 
  - **@params**:
    - _resultName_:
      - string with name of the file, the result is written to
    - _outputVarList_:
      - array with the wanted variables
  - values are read from the file with the name `resultName`
  - returns an numpy.array containing the trajectories for the variables in outputVarList using DymolaInterface

### **dymGetResultVarNames(self, resultName = "simResult")**
  - **@params**:
    - _resultName_:
      - string with name of the file, the result is written to
  - returns the variable names from the simulation result

### **dymClose(self)**
  - **@params**: none
  - closes the Dymola session
  - sets the DymolaObject `self.__dymObj` to `None`

from py4mod.modelicafunctions import ModelicaModel
from math import sqrt, cos, atan2
import re

#------------------- Script Settings -------------------------------
projectpath = "D:\\Users\\mmi-lne\\git\\ModPowerSystems"
#model = "ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Source_ZLoad"
#model = "ModPowerSystems.PhasorSinglePhase.Examples.ReferenceGrids.MV_Grid_ZLoad"
#model = "ModPowerSystems.PhasorSinglePhase.Examples.ReferenceGrids.MV_Grid_ZLoad_RxLines"
model = "ModPowerSystems.PhasorSinglePhase.Examples.ReferenceGrids.MV_Grid_PQLoad"
#model = "ModPowerSystems.PhasorSinglePhase.Examples.ReferenceGrids.MV_Grid_PQLoad_RxLines"


#-------------------- Script ---------------------------------------
#Open Model and Dymola and create a copy of the model
mo = ModelicaModel(model, projectpath)

name = ""
regex = re.search(r"\.(\w+)$", model)
if regex:
    name = regex.group(1)

mo.copyModelIntoPackage(name + "_Init_DynPhasor")
mo.dymOpen()

#Simulate Model and get results
mo.dymSimulate()
varNames = mo.dymGetResultVarNames()
trajectory = mo.dymGetResultsFast(outputVarList=varNames)
#search relevant variables for initialization    
initVals = []

for i in range(0, len(varNames)):
    var = varNames[i]
    endings = ["v.re", "v.im", "i.re", "i.im", "i_rx.re", "i_rx.im"]
    
    for ending in endings:
        regex = re.search(re.escape(ending) + r"$", var)
        if regex:
            initVals.append((str(var) + ".start", trajectory[i][0]))
    
print(initVals)      
initVals.sort()

mo.changeVars(initVals)

#Transform Phasor-Model into DynPhasor-Model
SubModelTypes = mo.getSubModelTypesAll()
for smt in SubModelTypes:
    regex = re.search(r"^ModPowerSystems", smt)
    smt_new = smt
    if  regex:
        smt_new = re.sub(r"\.PhasorSinglePhase\.", ".DynPhasorSinglePhase." , smt)
    else:
        regex = re.subn(r"^PhasorSinglePhase\.", "DynPhasorSinglePhase." , smt)
        if(regex[1] == 1):
            smt_new = regex[0]
        else:
            smt_new = "DynPhasorSinglePhase." + smt
    mo.changeSubmodelTypeAll(smt, smt_new)
    
mo.dymClose()
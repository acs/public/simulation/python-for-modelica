# Multilevel Examples

## Working
 - ZLoad_PiLine_DynPhasor
 - ZLoad_RxLine_DynPhasor
 - PQLoad_RxLine_DynPhasor
 - ZLoads_RxLine_EMT
 
## Not Working
 - PQLoad_PiLine_DynPhasor: aborts after short time(singularity)
 - ZLoad__PiLine_EMT: singular at 0
 - PQLoad_PiLine_EMT: model singular (doesn't even translate)
 - PQLoad_RxLine_EMT: model singular (doesn't even translate)


# Initialization Scripts
These scripts can be used to initalize different types of grid models:
 - Dynamic Phasor Models
 - EMT Models
 - EMT Models that contain PQLoads  

Before you can execute any of these Scripts you have
to specifiy the variables `projectpath` and `model` in the "Script Settings" section.
The variable `projectpath` should contain the path to your ModPowerSystems directory and 
`model`the full name of your model. e.g.  
``` python
    projectpath = "D:\\Users\\<username>\\git\\ModPowerSystems"
    model = "ModPowerSystems.PhasorSinglePhase.Examples.ReferenceGrids.MV_Grid_PQLoad"
```

## Initalize_DynPhasor
This Script initalizes Dynamic Phasor Models. The script initalizes the model by
simulating the phasor model in steady-state and then setting the resulting values of variables
with the ending
 - v.re
 - v.im
 - i.re
 - i.im
 - i_rx.re
 - i_rx.im

as inital values for the DynPhasor model. 

In detail this is obtained by simulating the model and then retrieving a list of
all variables in the model and a list of matching values to these variables.
The variable list seasrched for variables with the specified endings. The found ones
are then saved with the appndix `.start` in a new list of tuples. These tuples are pairs of the variables
and their values. The appendix shows that the values are start/initial values.
For `ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Source_ZLoad` e.g.:

``` python
[('N0.T.i.im.start', 0.0), ('N0.T.i.re.start', 0.0), ('N0.T.v.im.start', 0.0), ('N0.T.v.re.start', 11547.006), ('slack.T.i.im.start', -57.735027), ('slack.T.i.re.start', 144.33757), ('slack.T.v.im.start', 0.0), ('slack.T.v.re.start', 11547.006), ('slack.TGround.i.im.start', 0.0), ('slack.TGround.i.re.start', 0.0), ('slack.TGround.v.im.start', 0.0), ('slack.TGround.v.re.start', 0.0), ('slack.i.im.start', -57.735027), ('slack.i.re.start', 144.33757), ('slack.v.im.start', 0.0), ('slack.v.re.start', 11547.006), ('zLoad.T.i.im.start', -57.735027), ('zLoad.T.i.re.start', 144.33757), ('zLoad.T.v.im.start', 0.0), ('zLoad.T.v.re.start', 11547.006), ('zLoad.TGround.i.im.start', 0.0), ('zLoad.TGround.i.re.start', 0.0), ('zLoad.TGround.v.im.start', 0.0), ('zLoad.TGround.v.re.start', 0.0), ('zLoad.i.im.start', -57.735027), ('zLoad.i.re.start', 144.33757), ('zLoad.v.im.start', 0.0), ('zLoad.v.re.start', 11547.006)]
```

With this list the function `changeVars` from `py4mod.modelicafunctions` gets called.
This function writes the values for all variables in the list into the modelfile.

At the end the Phasor model gets transformed into a DynPhasor model by replacing
`PhasorSinglePhase` with `DynPhasorSinglePhase` in every submodeltype. This is done by getting
a list of all submodeltypes with `getSubModelTypesAll()` which then get edited and
finally the old Phasor-submodeltype `smt` gets replaced with the new dynphasor one
`smt_new` by calling `changeSubModelTypeAll()`.


## Initalize_EMT
This Script initalizes EMT Models that don't contain PQLoads. The script initalizes the model by
simulating the phasor model in steady-state and then using the resulting values of variables
with the ending
 - v.re
 - v.im
 - i.re
 - i.im
 - i_rx.re
 - i_rx.im

to calculate the initial values for the EMT model. 

In detail this is obtained by simulating the model and then retrieving a list of
all variables in the model and a list of matching values to these variables.
The variable list is searched for variables with the specified endings. The found ones
are then saved with the appendix `.start` in a new list the same is done for their values.
With these values the initial values get calculated and stored together with their
variable names in a list of tuples. The appendix of the variable names shows that 
the values are start/initial values.
For `ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Source_ZLoad` e.g.:

``` python
[('N0.T.i.re.start', 0.0), ('N0.T.i.im.start', 0), ('N0.T.v.re.start', 11547.005859175299), ('N0.T.v.im.start', 0), ('slack.T.i.re.start', 144.3379097423535), ('slack.T.i.im.start', 0), ('slack.T.v.re.start', 11547.005859175299), ('slack.T.v.im.start', 0), ('slack.TGround.i.re.start', 0.0), ('slack.TGround.i.im.start', 0), ('slack.TGround.v.re.start', 0.0), ('slack.TGround.v.im.start', 0), ('slack.i.re.start', 144.3379097423535), ('slack.i.im.start', 0), ('slack.v.re.start', 11547.005859175299), ('slack.v.im.start', 0), ('zLoad.T.i.re.start', 144.3379097423535), ('zLoad.T.i.im.start', 0), ('zLoad.T.v.re.start', 11547.005859175299), ('zLoad.T.v.im.start', 0), ('zLoad.TGround.i.re.start', 0.0), ('zLoad.TGround.i.im.start', 0), ('zLoad.TGround.v.re.start', 0.0), ('zLoad.TGround.v.im.start', 0), ('zLoad.i.re.start', 144.3379097423535), ('zLoad.i.im.start', 0), ('zLoad.v.re.start', 11547.005859175299), ('zLoad.v.im.start', 0)]
```

With this list the function `changeVars` from `py4mod.modelicafunctions` gets called.
This function writes the values for all variables in the list into the modelfile.

The transformation from the Phasor model to a EMT model is analogous to DynPhasor.

## Initalize_EMT
This Script initalizes EMT Models that contain PQLoads. The script initalizes the model by
simulating the phasor model in steady-state and then using the resulting values of variables
with the Submodeltype `PQLoad` and the ending
 - v.re
 - v.im

to calculate the paramers `Vnom` and `phase` for the PQLoads. 

In detail this is obtained by simulating the model and then retrieving a list of
all variables in the model and a list of matching values to these variables.
Additionaly a list of varriables that have the type `PQLoad` is retrieved from the model
by calling `getSubmodelNames("PQLoad")`. This list plus the endings are used to find
the postion of the needed values in the value list and extract them into a new one.
With these values `Vnom` and `phase` get calculated for all PQLoads and stored together with their
variable names in a list of tuples. 
For `ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Source_PQLoad` e.g.:

``` python
[['pQLoad.Vnom', 11547.005859375], ['pQLoad.phase', 0.0]]
```

With this list the function `changeVars` from `py4mod.modelicafunctions` gets called.
This function writes the values for all variables in the list into the modelfile.
Then the parametervalues for `Pnom` and `Qnom` are deleted for all PQLoads by putting them 
in a list and calling `deleteValues` with this list as argument.
After that all `PQLoads` in the modelfile get texchanged for  `TransPQloads` by calling
`changeSubmodelTypeAll("Loads.PQLoad", "Converter.TransPQLoad").

The transformation from the Phasor model to a EMT model is analogous to DynPhasor.


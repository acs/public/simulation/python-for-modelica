from py4mod.modelicafunctions import ModelicaModel
from math import sqrt, cos, atan2
import re

#------------------- Script Settings -------------------------------
projectpath = "D:\\Users\\mmi-lne\\git\\ModPowerSystems"
#model = "ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Source_ZLoad"
#model = "ModPowerSystems.PhasorSinglePhase.Examples.ReferenceGrids.MV_Grid_ZLoad"
#model = "ModPowerSystems.PhasorSinglePhase.Examples.ReferenceGrids.MV_Grid_ZLoad_RxLines"


#------------------- Script ----------------------------------------

#Open Model and Dymola
mo = ModelicaModel(model, projectpath)

name = ""
regex = re.search(r"\.(\w+)$", model)
if regex:
    name = regex.group(1)

mo.copyModelIntoPackage(name + "_Init_EMT")
mo.dymOpen()

#Simulate Model and get results
mo.dymSimulate()
varNames = mo.dymGetResultVarNames()
trajectory = mo.dymGetResultsFast(outputVarList=varNames)

#search relevant variables for initialization    
simVars = []    
simVals = []

for i in range(0, len(varNames)):
    var = varNames[i]
    endings = ["v.re", "v.im", "i.re", "i.im", "i_rx.re", "i_rx.im"]
    
    for ending in endings:
        regex = re.search(re.escape(ending) + r"$", var)
        if regex:
            simVars.append(str(var) + ".start")
            simVals.append(trajectory[i][0])

omega = trajectory[varNames.index("system.omega")][0]

#calculate initial values
initVals = []
for i in range(0, len(simVars), 2):
    Abs = sqrt(simVals[i+1]**2 + simVals[i]**2)
    Phi = atan2(simVals[i], simVals[i+1])
    val = Abs*cos(omega + Phi)
    initVals.append((simVars[i+1], val))
    initVals.append((simVars[i], 0))
    
print(initVals)
initVals.sort()

mo.changeVars(initVals)

#Transform Phasor-Model into EMT-Model
SubModelTypes = mo.getSubModelTypesAll()
for smt in SubModelTypes:
    regex = re.search(r"^ModPowerSystems", smt)
    smt_new = smt
    if  regex:
        smt_new = re.sub(r"\.PhasorSinglePhase\.", ".EmtSinglePhase." , smt)
    else:
        regex = re.subn(r"^PhasorSinglePhase\.", "EmtSinglePhase." , smt)
        if(regex[1] == 1):
            smt_new = regex[0]
        else:
            smt_new = "EmtSinglePhase." + smt
    mo.changeSubmodelTypeAll(smt, smt_new)
    
mo.dymClose()

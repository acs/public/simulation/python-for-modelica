from py4mod.modelicafunctions import ModelicaModel
from math import sqrt, atan2
import re

#Modelinformation:
projectpath = r"D:\Users\mmi-lne\git\ModPowerSystems"
#model = "ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Source_PQLoad"
#model = "ModPowerSystems.PhasorSinglePhase.Examples.ReferenceGrids.MV_Grid_PQLoad"
model = "ModPowerSystems.PhasorSinglePhase.Examples.ReferenceGrids.MV_Grid_PQLoad_RxLines"

#Open Model and Dymola
mo = ModelicaModel(model, projectpath)

name = ""
regex = re.search(r"\.(\w+)$", model)
if regex:
    name = regex.group(1)

mo.copyModelIntoPackage(name + "_Init_EMT")
mo.dymOpen()

#Simulate Model and get results
mo.dymSimulate()
varNames = mo.dymGetResultVarNames()
trajectory = mo.dymGetResultsFast(outputVarList=varNames)

#search relevant variables for initialization  
simVars = []    
simVals = []

subModelNames = mo.getSubmodelNames("PQLoad")
subModelNames.sort()

for i in range(0, len(subModelNames), 1):
    simVars.append(subModelNames[i] + ".v.im")
    pos = varNames.index(subModelNames[i] + ".v.im")
    simVals.append(trajectory[pos][0])
    
    simVars.append(subModelNames[i] + ".v.re")
    pos = varNames.index(subModelNames[i] + ".v.re")
    simVals.append(trajectory[pos][0])

#calculates new parameters
initVals = []
for i in range(0, len(simVars), 2):
    Abs = sqrt(simVals[i+1]**2 + simVals[i]**2)
    Phi = atan2(simVals[i], simVals[i+1])
    initVals.append([subModelNames[i/2] + ".Vnom", Abs])
    initVals.append([subModelNames[i/2] + ".phase", Phi])

print(initVals)
mo.changeVars(initVals)

rmList = []
for item in subModelNames:
    rmList.append(item + ".Pnom")
    rmList.append(item + ".Qnom")
    
mo.deleteValues(rmList)
mo.changeSubmodelTypeAll("Loads.PQLoad", "Converter.TransPQLoad")

#Transform Phasor-Model into EMT-Model
SubModelTypes = mo.getSubModelTypesAll()
for smt in SubModelTypes:
    regex = re.search(r"^ModPowerSystems", smt)
    smt_new = smt
    if  regex:
        smt_new = re.sub(r"\.PhasorSinglePhase\.", ".EmtSinglePhase." , smt)
    else:
        regex = re.subn(r"^PhasorSinglePhase\.", "EmtSinglePhase." , smt)
        if(regex[1] == 1):
            smt_new = regex[0]
        else:
            smt_new = "EmtSinglePhase." + smt
    mo.changeSubmodelTypeAll(smt, smt_new)
    
mo.dymClose()
      
import os
import sys

from py4mod.ModelicaModel import ModelicaModel

def simulate_modpowersystems_model(model_name, model_path):
    interface = ModelicaModel(model_name, model_path)

    # Initialization
    interface.createInterface("OPENMODELICA")
    interface.loadFile("./ModPowerSystems/package.mo")

    # Redirection
    cwd = os.getcwd()
    wd = os.path.join(cwd, model_name[model_name.rfind('.') + 1:])
    if not os.path.exists(wd):
        os.makedirs(wd)
    interface.changeWorkingDirectory(wd.replace("\\", "/"))

    # Build & Run
    interface.buildModel()
    interface.simulate()


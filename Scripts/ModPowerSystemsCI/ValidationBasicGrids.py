#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os,sys,getopt

from RunOMSimulationCI import simulate_modpowersystems_model
from villas.dataprocessing.readtools import *
from villas.dataprocessing.validationtools import *
from py4mod import *

"""
name = ['ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Slack_PiLine_PQLoad',
        'ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Slack_RxLine_PQLoad',
        'ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Slack_RxLine_PVNode',
        'ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Slack_Tra_Line_PQLoad',
        'ModPowerSystems.PhasorSinglePhase.Examples.BasicGrids.Slack_ZLoad']

path = './modpowersystems'

for name_mod in name:
    simulate_modpowersystems_model(name_mod, path)


####################### Assert ######################

for name_mod in name:
    print(os.path.abspath(name_mod[name_mod.rfind('.')+1:] + "/" + name_mod + "_res.mod"))
    validate_modelica_res(name_mod,
                          os.path.abspath(name_mod[name_mod.rfind('.')+1:] + "/" + name_mod + "_res.mat"),
                          os.path.abspath("reference-results/Neplan/BasicGrids/" + name_mod[name_mod.rfind('.')+1:] + ".rlf"))
"""

def validation_mps_basic_grids(argv):
    name = ''
    path = ''
    reference = ''
    try:
        opts, args = getopt.getopt(argv, "hp:n:r:", ["path=", "name=", "reference="])
    except getopt.GetoptError:
        print('ValidationBasicGrids.py -p <path> -n <name> -r <reference-result>')
        sys.exit()
    for opt, arg in opts:
        if opt == ("-h", "--help"):
            print('ValidationBasicGrids.py -p <path> -n <name> -r <reference-result>')
            sys.exit()
        elif opt in ("-p", "--path"):
            path = arg
        elif opt in ("-n", "--name"):
            name = arg
        elif opt in ("-r", "--reference"):
            reference = arg
    print("Path: " + path)
    print("Model under validation: " + name)
    print("Reference results: " + reference)
    simulate_modpowersystems_model(name,path)
    validate_modelica_res(name, os.path.abspath(name[name.rfind('.') + 1:] + "/" + name + "_res.mat"),
                          os.path.abspath(reference))


if __name__ == '__main__':
    validation_mps_basic_grids(sys.argv[1:])
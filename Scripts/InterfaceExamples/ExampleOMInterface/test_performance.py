"""
comparison between the time used by the function simulate() and simulateModel() to simulate 5 times 25s the model "Modelica.Blocks.Examples.PID_Controller"
"""

import time
import os
import sys
sys.path.append("C:/Users/Martin/Desktop/hiwi/git/python-for-modelica/Py4Mod/py4mod")

from OMInterface import OMInterface

#Instantiate OMInterface
print("")
modelica = OMInterface()
print("")

#load Modelica-package 
try:
	modelica.loadLibrary("Modelica")

	#change working directory
	cwd = os.getcwd()
	wd=os.path.join(cwd, 'test_performance')
	if not os.path.exists(wd):
		os.makedirs(wd)
	modelica.changeWorkingDirectory(wd.replace("\\","/"))
	
	print("using the funcion simulateModel()...")
	start0 = time.time()
	for x in range(0, 5):
		start = time.time()
		print('starts {}.simulation'.format(x+1))

		#simulate model
		modelica.simulateModel("Modelica.Blocks.Examples.PID_Controller", startTime=0.0, stopTime=25.0,
			simflags="-override=spring.w_rel=10,PI.Dzero.k=5.0")
	
		end = time.time()
		print('time elapsed {}.simulation: {}seg'.format(x+1, end - start))
	
	end = time.time()
	print('total time elapsed: {}'.format(end - start0))
	
	
	print("\n======================\n")
	
	print("using the function simulate()...")
	start0 = time.time()
	
	for x in range(0, 5):
		start = time.time()
		print('starts {}.simulation'.format(x+1))
		if x==0:
			#build model
			modelica.buildModel("Modelica.Blocks.Examples.PID_Controller")		
		
		#create the file with the initial values:
		initFile=os.path.join(wd, 'init_values.csv')
		f = open(initFile,'w')
		file='startTime=0 \n stopTime=25 \n spring.w_rel=10.0 \n PI.Dzero.k=5.0 \n'.replace(" ", "")
		f.write(file)	
		f.close()
	
		sim_flags='-overrideFile {}'.format(initFile).replace("\\","/")
		modelica.simulate(simflags=sim_flags)
	
		end = time.time()
		print('time elapsed {}.simulation: {}seg'.format(x+1, end - start))
		
	print('total time elapsed: {}'.format(end - start0))
	print("\n======================")
	
	#check start value of spring.w_rel and PI.Dzero.k and time
	test = modelica.getResults(["spring.w_rel", "PI.Dzero.k", "time"])
	print('\nspring.w_rel[0]: {}'.format(test[0][0]))
	print('PI.Dzero.k[0]: {}'.format(test[1][0]))
	print('time: {}'.format(test[2]))
	print("\n")
		

except Exception as err:
	print(err)
	exit()
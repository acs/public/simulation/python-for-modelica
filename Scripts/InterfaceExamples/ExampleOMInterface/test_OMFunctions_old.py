#test of OMFunctions.py
import os
import sys
sys.path.append(r"C:\\Users\\Martin\\Desktop\\hiwi\\git\\python-for-modelica\\Py4Mod\\py4mod")

from OMFunctions_old import OMInterface

#Instantiate OMInterface
print("")
modelica = OMInterface()
print("")

#load Modelica-package 
try:
	modelica.loadLibrary("Modelica")
except Exception as err:
	print(err)
	
#print loaded packages
print("Loaded Packages:")
print(*modelica.getPackages(), sep='\n')
print("")

#change working directory
cwd = os.getcwd()
wd=os.path.join(cwd, 'test')
if not os.path.exists(wd):
	os.makedirs(wd)
modelica.changeWorkingDirectory(wd.replace("\\","/"))
	
#check the current working directory 
print('current working directory: "{}"\n'.format(modelica.changeWorkingDirectory("")))

#build model
model = "Modelica.Blocks.Examples.PID_Controller"
try:
	modelica.buildModel(model)
except Exception as err:
	print(err)
	exit()
	
print("\nget component names")
print(modelica.getComponents())
#print(*modelica.getComponents(), sep='\n')

#change sim option values
modelica.setSimulationOptions(stopTime=10, stepSize=20)

#get simulation values
simNamesList = ['startTime', 'stopTime', 'stepSize', 'tolerance', 'solver']
simValues = modelica.getSimulationValues()
print("\nsimulation values:")
for index, value in enumerate(simValues):
	print('{}={}'.format(simNamesList[index], value))
print("")

#set spring.w_rel to 10 deg:
modelica.setValue({"spring.w_rel":10})

#simulate model
print("Simulate model:")
modelica.simulate()

#get output variables
print("\noutput variables:")
print(*modelica.getResultVarNames(), sep='\n')
print("\n")

#get solutions
print("get simulation results:")
test = modelica.getResults(["time", "PI.I.y","inertia1.phi"])
print('time: ')
print(test[0])
print("\n")
print('PI.I.y: ')
print(test[1])
print("")

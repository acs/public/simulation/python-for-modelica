"""
example of how to use the function simulateModel and the sim flag -override
"""

import os
import sys
sys.path.append(r"C:\\Users\\Martin\\Desktop\\hiwi\\git\\python-for-modelica\\Py4Mod\\py4mod")

from OMInterface import OMInterface

#Instantiate OMInterface
print("")
modelica = OMInterface()
print("")

#load Modelica-package 
try:
	modelica.loadLibrary("Modelica")
except Exception as err:
	print(err)
	

#example how to set init values with the sim flag -override
try: 
	#change working directory
	cwd = os.getcwd()
	wd=os.path.join(cwd, 'test_simflag_override')
	if not os.path.exists(wd):
		os.makedirs(wd)
	modelica.changeWorkingDirectory(wd.replace("\\","/"))

	#build model
	print("Build model Modelica.Blocks.Examples.PID_Controller")
	modelica.buildModel("Modelica.Blocks.Examples.PID_Controller", startTime=5, stopTime=10)


	#simulate model
	#you can edit the initial values with the sim flag -override
	modelica.simulate(simflags="-override=spring.w_rel=20,PI.Dzero.k=5.0,stopTime=50")

	
	#check start value of spring.w_rel and PI.Dzero.k
	test = modelica.getResults(["spring.w_rel", "PI.Dzero.k", "time"])
	print('\nspring.w_rel[0]: {}'.format(test[0][0]))
	print('PI.Dzero.k[0]: {}'.format(test[1][0]))
	print('time: {}'.format(test[2]))
	print("\n")
	
except Exception as err:
	print(err)
	


"""
example of how to use the function simulateModel and set initial values from a file
"""

import os
import sys
sys.path.append("C:/Users/Martin/Desktop/hiwi/git/python-for-modelica/Py4Mod/py4mod")

from OMInterface import OMInterface

#Instantiate OMInterface
print("")
modelica = OMInterface()
print("")

#load Modelica-package 
try:
	modelica.loadLibrary("Modelica")
except Exception as err:
	print(err)
	
#example how to set initial values from a file
try: 
	#change working directory
	cwd = os.getcwd()
	wd=os.path.join(cwd, 'test_simflag_overrideFile')
	if not os.path.exists(wd):
		os.makedirs(wd)
	modelica.changeWorkingDirectory(wd.replace("\\","/"))
	
	#build model
	print("Build model Modelica.Blocks.Examples.PID_Controller")
	modelica.buildModel("Modelica.Blocks.Examples.PID_Controller")
	
	#simulate model
	#you can use one file to set initial values
	#Note that: -overrideFile CANNOT be used with -override. Use when variables for -override are too many.
	
	#create the file with the initial values:
	#it is important that the parameter file concludes with a blank line and that no
	#spaces can be inserted before or after the equal sign, otherwise the
	#OpenModelica compiler does not inherit the parameters in the model.
	initFile=os.path.join(wd, 'init_values.csv')
	f = open(initFile,'w')
	file='startTime=0 \n stopTime=30 \n spring.w_rel=20.0 \n PI.Dzero.k=10.0 \n'.replace(" ", "")
	f.write(file)	
	f.close()

	sim_flags='-overrideFile {}'.format(initFile).replace("\\","/")
	print("Simulate Model:")
	modelica.simulate(simflags=sim_flags)
	
	#check start value of spring.w_rel and PI.Dzero.k and time
	test = modelica.getResults(["spring.w_rel", "PI.Dzero.k", "time"])
	print('\nspring.w_rel[0]: {}'.format(test[0][0]))
	print('PI.Dzero.k[0]: {}'.format(test[1][0]))
	print('time: {}'.format(test[2]))
	print("\n")

except Exception as err:
	print(err)

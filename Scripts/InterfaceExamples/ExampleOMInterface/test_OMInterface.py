#test of OMInterface.py
import os
import sys
import time
import numpy as np

sys.path.append(r"C:\\Users\\Martin\\Desktop\\hiwi\\git\\python-for-modelica\\Py4Mod\\py4mod")
from OMInterface import OMInterface

#Instantiate OMInterface
print("")
modelica = OMInterface()
print("")

#load Modelica-package 
try:
	modelica.loadLibrary("Modelica")
except Exception as err:
	print(err)
	
#print loaded packages
print("Loaded Packages:")
print(*modelica.getPackages(), sep='\n')
print("")

#change working directory
cwd = os.getcwd()
wd=os.path.join(cwd, 'test_OMInterface')
if not os.path.exists(wd):
	os.makedirs(wd)
modelica.changeWorkingDirectory(wd.replace("\\","/"))
	
#check the current working directory 
print('current working directory: "{}"'.format(modelica.changeWorkingDirectory("")))

#build model
model = "Modelica.Blocks.Examples.PID_Controller"
print("Build Model:")
try:
	modelica.buildModel(model, startTime=5, stopTime=10)
except Exception as err:
	print(err)
	exit()
	
#simulate model
print("\nSimulate model:")
modelica.simulate('-override spring.w_rel=12')

#get output variables
print("\noutput variables:")
print(*modelica.getResultVarNames(), sep='\n')

#get solutions
start = time.time()
print("\nget simulation results:")
test = modelica.getResults(["time", "PI.I.y","spring.w_rel"])
print(test)
end = time.time()
print('time elapsed: {}'.format(end-start))

#get solutions
start = time.time()
print("\nget simulation results fast:")
test = modelica.getResultsFast(["time", "PI.I.y","spring.w_rel"])
print(test)
end = time.time()
print('time elapsed: {}'.format(end-start))

#test setValue():
values={"spring.w_rel":30}
modelica.setValue(values)
modelica.simulate()
test = modelica.getResults(["spring.w_rel"])
print('spring.w_rel[0]={}'.format(test[0][0]))

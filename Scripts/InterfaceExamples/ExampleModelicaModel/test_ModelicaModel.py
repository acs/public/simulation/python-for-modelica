#test of ModelicaFunctions.py
import os
import sys
sys.path.append(r"C:\\Users\\Martin\\Desktop\\hiwi\\git\\python-for-modelica\\Py4Mod\\py4mod")

from ModelicaModel import ModelicaModel

try:
	#Instantiate ModelicaModel
	interface = ModelicaModel("ModPowerSystems.DynPhasorThreePhase.Examples.BasicCircuits.VoltageSource_RL", r"C:\\Users\\Martin\\Desktop\\hiwi\\git\\modpowersystems\\")
	
	#initialize the OM interface
	interface.createInterface("OPENMODELICA")
	
	#load ModPowerSystems package
	interface.loadFile(r"C:/Users/Martin/Desktop/hiwi/git/modpowersystems/ModPowerSystems/package.mo")
	
	#print loaded packages
	print("\nLoaded Packages:")
	print(*interface.interface.getPackages(), sep='\n')
	print("")
	
	#change working directory
	cwd = os.getcwd()
	wd=os.path.join(cwd, 'test_modelicaFunctions')
	if not os.path.exists(wd):
		os.makedirs(wd)
	interface.changeWorkingDirectory(wd.replace("\\","/"))
	
	#create a copy of the model
	interface.copyModelIntoPackage("VoltageSource_RL_copy")
	
	#get all submodeltypes that the model contains
	print("submodels:")
	print(interface.getSubModelTypesAll())
	
	#get sub models type ModPowerSystems.DynPhasorThreePhase.Basics.Resistor
	print(interface.getSubmodelNames("Resistor"))
	
	#get value of resistors
	#print('start value resistor 1: {}'.format(interface.getValues(["ModPowerSystems.DynPhasorThreePhase.Basics.Resistor.R[1]"])))
	#print('start value resistor 2:{}'.format(interface.getValues(["resistor.R[2]"])))
	#print('start value resistor 3:{}'.format(interface.getValues(["resistor.R[3]"])))
	
	#build model
	print("\nBuild model ModPowerSystems.DynPhasorThreePhase.Examples.BasicCircuits.VoltageSource_RL")
	interface.buildModel(startTime=5, stopTime=10, simflags="-override=resistor.R[2]=10")
	
	
	#simulate model
	
	#create the file with the initial values:
	initFile=os.path.join(wd, 'init_values.csv')
	f = open(initFile,'w')
	file='startTime=0 \n stopTime=30 \n resistor.R[1]=10 \n resistor.R[2]=20 \n resistor.R[3]=30 \n'.replace(" ", "")
	f.write(file)	
	f.close()
	
	#it is important that the parameter file concludes with a blank line and that no
	#spaces can be inserted before or after the equal sign, otherwise the
	#OpenModelica compiler does not inherit the parameters in the model.
	
	sim_flags='-overrideFile {}'.format(initFile).replace("\\","/")
	print("\nSimulate Model:")
	interface.simulate(simflags=sim_flags)
		
	#check start value of the resistors
	test = interface.getResults(["resistor.R[1]", "resistor.R[2]", "resistor.R[3]", "time"])
	print('\nresistor.R[1]: {}'.format(test[0][0]))
	print('resistor.R[2]: {}'.format(test[1][0]))
	print('resistor.R[3]: {}'.format(test[2][0]))
	print('time: {}'.format(test[3]))
	print("\n")
		
	print(test)
	

except Exception as err:
	print(err)
	exit(1)
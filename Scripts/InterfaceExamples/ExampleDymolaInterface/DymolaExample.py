"""
example of how to use the DymolaInterface
"""

from py4mod.DymInterface import DymInterface

#Instantiate the Dymola interface and start Dymola
dymola = DymInterface(showwindow=True)
print("")

try:
    # Call a function in Dymola and check its return value
    result = dymola.simulateModel("Modelica.Mechanics.Rotational.Examples.CoupledClutches")

    print('cwd:{}\n'.format(dymola.changeWorkingDirectory()))
    print("VarNames:")	
    print(dymola.getResultVarNames())
    print("\n\n\n")	
	
    print("J1.J")	
    print(dymola.getResults(["J1.J", "fixed.flange.phi"]))
    print("\n\n\n")

    print("J1.J")	
    print(dymola.getResultsFast(["J1.J", "fixed.flange.phi"]))
    print("\n\n\n")
	
except Exception as err:
    print("Error: " + str(err))
	
finally:
    if dymola is not None:
        dymola.close()
        dymola = None

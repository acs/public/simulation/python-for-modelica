# General
This project is a collection of Python scripts which can be used to change Modelica code in an automatic way.

#### Function overview/ TODOs
- [Function overview](./functionOverview.md)
- [TODOs overview](./todos.md)
- [Blueprints](./blueprint.md)

# Setup
## Users
In the top level directory run: 
```python
python setup.py install
```

## Developers
In the top level directory run: 
```python
python setup.py develop
```


# List of Contents
1. ModelicaFunctions
    1. [ModelicaModel](#ModelicaModel)
2. Multilevel
    1. [InitalizeZLoad](#InitalizeZLoad)
    2. [InitalizePQLoad](#InitalizePQLoad)
3. CoSimulation
	1. [Data](#Data)

		
# <a name="ModelicaModel"></a>ModelicaModel
## Description
The class ModelicaModel allows it to edit Modelica-Models out of python scripts, to achieve this it provides different functions. These are:
* copyModel
* getSubModelNames
* changeSubModelType
* changeVars
* getValues
* deleteValues
* addConnection
* getConnections  

## Examples
__note:__ The changes i make in the model don't have to make any sense, they are just there to show you how you can use the functions. The resulting model won't even compile because i add connections and change variabletypes in a senseless way.  


For the Examples we use the model Line_PQLoad:
```
model Line_PQLoad

  Sources.Slack             slack(Vnom=20e3)
    a;
  inner System                  system(simMode="steady")
    a;
  Lines.PiLine             piLine(b=0)
                                  a;
  Connections.BusBar             N0(Vnom=20e3)
    a;
  Loads.PQLoad pQLoad(
    Vnom=20e3,
    Pnom=5e6,
    Qnom=2e6)
    a;
  Connections.BusBar             N1(Vnom=20e3)
    a;
equation 
  connect(piLine.T1,slack. T)
    a;
  connect(piLine.T1,N0. T)
    a;
  connect(pQLoad.T, piLine.T2)
    a;
  connect(pQLoad.T, N1.T)
    a;
  a;
end Line_PQLoad;
```
Its full name in the Project ModPowerSystem is `ModPowersystems.SinglePhase.Examples.StatPH_Examples.Line_PQLoad` and it is stored in e.g. `C:\Users\john-doe\git\ModPowerSystems\ModPowerSystems\SinglePhase\Examples\StatPH_Examples\Line_PQLoad.mo`.   

### constructor
To create an instance of this model we just have to pass the projectpath and the full modellname to the constructor:  
 ``` python
projectpath  =  r"C:\Users\john-doe\git\ModPowerSystems"  #use rawstring so that the backslash isn't interpreted as escape character
model = "ModPowersystems.SinglePhase.Examples.StatPH_Examples.Line_PQLoad"
modelicamodel = ModelicaModel(model, projectpath)
```

### dymOpen
To open an instance of Dymola you have to call:
``` python
modelicamodel.dymOpen
```

### dymTranslate
To translate the opened model you have to call:
``` python
modelicamodel.dymTranslate
```

### dymApplyModifiers
???
function implemented by jan

### dymSimulate
Call this function 

### getSubModelNames
To get the names of the Variables with the type BusBar you use the function `getSubModelNames(type)`:
``` python
busbars = modelicamodel.getSubModelNames("BusBar")
```
busbars is now a list that looks like this:
```python
["N0", "N1"]  
``` 

### getSubmodelModifierValue
???

### getSubmodelTypesAll
Returns a list with all submodeltypes that exist in the model. In our case this would be:
``` python
["Sources.Slack", "System", "Lines.PiLine", "Connections.BusBar", "Loads.PQLoad"]
```

### changeSubModelTypeAll
When you want to change the type of all `BusBar` instances to `BusBar_Data_out` you can use the function `changeSubModelType(oldType, newType)`:
```python
modelicamodel.changeSubModelType("BusBar", "BusBar_Data_out")
```
after the call of the function the variable section of thmodel looks like this:
```
  Sources.Slack             slack(Vnom=20e3)
    a;
  inner System                  system(simMode="steady")
    a;
  Lines.PiLine             piLine(b=0)
                                  a;
  Connections.BusBar_Data_out             N0(Vnom=20e3)
    a;
  Loads.PQLoad pQLoad(
    Vnom=20e3,
    Pnom=5e6,
    Qnom=2e6)
    a;
  Connections.BusBar_Data_out             N1(Vnom=20e3)
    a;
```

### changeSubModelType


### changeVars
 To change N0.Vnom to 30000 and the initial Value of piLine.T1.v.re to 9999, you create a list that contains the changes and call `changeVars(varList, overwrite = True)`with this list
```python
item1 = ["Vnom" , 30000]
item2 = ["piLine.T1.v.re.start", 9999] #you have to add .start to the variablename, because it's a differential variable
modelicamodel.changeVars([item1, item2])
```
After the function call the model looks like this:
```
model Line_PQLoad

  Sources.Slack             slack(Vnom=20e3)
    a;
  inner System                  system(simMode="steady")
    a;
  Lines.PiLine             piLine(b=0,T1(v(re(start=9999))))
                                  a;
  Connections.BusBar             N0(Vnom=30000)
    a;
  Loads.PQLoad pQLoad(
    Vnom=20e3,
    Pnom=5e6,
    Qnom=2e6)
    a;
  Connections.BusBar             N1(Vnom=20e3)
    a;
equation 
  connect(piLine.T1,slack. T)
    a;
  connect(piLine.T1,N0. T)
    a;
  connect(pQLoad.T, piLine.T2)
    a;
  connect(pQLoad.T, N1.T)
    a;
  a;
end Line_PQLoad;
```
If `overwrite` had been set as `False`, `N0.Vnom` would still be `20e3`, but because nothing is set and the default is `overwrite = True` it has been overwritten.

### getConnections
If you want to get every connection that the submodel piLine has you can call:
```python
[pin1, pin2] = modelicamodel.getConnections("piLine")
```
now pin1 and pin2 looks like this:
```
pin1: [piLine.T1, piLine.T1, piLine.T2]
pin2: [slack.T, N0.T, N1.T]
```
If you want the connections of the connector piLine.T2 you call:
```python
[pin1, pin2] = modelicamodel.getConnections("piLine.T2")
```
now pin1 and pin2 looks like this:
```
pin1: [piLine.T2]
pin2: [N1.T]
```

# <a name="InitalizeZLoad"></a>InitalizeZLoad
## Description
This Script generates a transient initalized model from models that are based on ZLoads, by simulating it in Steady-State and calculating the initialvalues from the simulation results. The calculated values get written in the new created transient model.
## How to use
To use this  on a model you have to specify its name and the projectpath of it in the script.

# <a name="InitalizePQLoad"></a>InitalizePQLoad
## Description
This Script generates a transient initalized model from models that are based on PQLoads, by simulating it in Steady-State and calculating the right parametervalues from the simulation results. The calculated values get written in the new created transient model and Loads.PQLoads get changed to Converter.TransPQLoad.
## How to use
To use this  on a model you have to specify its name and the projectpath of it in the script.

# <a name="Data"></a>Data
## Description
This script creates a model with Datain- and output, by replacing the System-Block with the System_Data, which can receive and send data from/to a server and connecting all System_Out- and In-blocks to the new system block.  
The mapping of the variables to their index is stored in mapping_in.csv and mapping_out.csv.
## How to use
To use this  on a model you have to specify its name and the projectpath of it in the script.

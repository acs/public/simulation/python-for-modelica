import os
import sys
import platform
import subprocess
import numpy as np
import xml.etree.ElementTree as ET
from modelicares import SimRes
from OMPython import OMCSessionZMQ

class ModelicaVariable(object):
	"""
	To represent modelica ModelVariables
	"""
	def __init__(self, name, start, changable, variability, description, causality, alias, aliasvariable):
		self.name = name
		self.start = start
		self.changable = changable
		self.description = description
		self.variability = variability
		self.causality = causality
		self.alias = alias
		self.aliasvariable = aliasvariable
	
	
class OMInterface(object):
	def __init__(self, modelica_path=None):
		"""
		create an OMCSessionZMQ object
		
		@param str modelica_path: The path to the directory "openmodelica_install_directory\lib\omlibrary". 
			Default is used the environment variable OPENMODELICALIBRARY
		"""
	
		#create an OMCSessionZMQ object
		self.__omc = OMCSessionZMQ()
		
		#set dymola path
		if modelica_path is not None:
			self.setModelicaPath(modelica_path)
		
		#model variables
		self.modelName=""
		self.resultFileName=""
		
		#variables to describe the modelName_init.xml file.
		#This is the file that description the building model
		self.__xmlFile=None			#Modelica xml Representation 
		self.__xmlTree=None			#xml File tree structure
		self.__xmlRoot=None			#xml File root element
		
		#list of variables with detailes such as name, value, changeable, and description, 
		#where changeable means if value for corresponding quantity name is changeable or not.	
		#To describe each variable, we use the class ModelicaVariable()
		self.__variablesList=[]		

		#class flags
		self.__buildFlag=False
		self.__simulationFlag=False
	
	
	def changeWorkingDirectory(self, dir=""):
		"""
		Change directory to the given path (which may be either relative or absolute) 
		If the given path is the empty string, the function simply returns the current working directory
		
		@param str Dir: Directory to change to. If directory="" this function return the current working directory
		@raises: exception
		"""
		if dir=="":
			exp='cd("")'
			return self.__omc.sendExpression(exp)
		
		if not os.path.exists(dir):
			raise Exception('File Error: {} does not exists!!!'.format(os.path.abspath(dir)))
		
		exp = 'cd("{}")'.format(str(dir))
		res=self.__omc.sendExpression(exp)
		
		if not (res==dir):
			raise Exception(res)
		
		
	def loadLibrary(self, lName):
		"""
		Loads a Modelica library from the path indicated by the
		environment variable OPENMODELICALIBRARY (e.g. in C:\OpenModelica1.12.0-64bit\lib\omlibrary)
		You can use the method OMInterface.getAvailableLibraries to see all available libraries in OPENMODELICALIBRARY
		
		@param str lName: the model name to load
		@raises: Exception()
		"""
		if lName not in (self.getAvailableLibraries()):
			raise Exception('The library "{}" was not found in the environment variable OPENMODELICALIBRARY'.format(lName))
	
		loadmodelError = ''
		loadModelResult = self.__requestApi("loadModel", lName)
		loadmodelError = self.__requestApi('getErrorString')
		if loadmodelError:
			raise Exception(loadmodelError)
			
			
	def getAvailableLibraries(self):
		"""
		Looks for all libraries that are visible from the getModelicaPath().
		@return a list with the abailable libraries
		"""
		return self.__requestApi("getAvailableLibraries")
	
	
	def loadFile(self, fName):
		"""
		Reads the file specified by fName.
		This corresponds to File->Open in the menus. 
		
		@param str fName: File-path to open.
		@raises: Exception()
		"""
		# if file does not exists
		if not os.path.exists(fName):  
			raise Exception("File Error: " + os.path.abspath(fileName) + " does not exists!!!")
			
		loadfileResult = self.__requestApi("loadFile", fName)
		loadfileError = self.__requestApi("getErrorString")
			
		if loadfileError:
			specError = 'Parser error: Unexpected token near: optimization (IDENT)'
			if specError in loadfileError:
				self.__requestApi("setCommandLineOptions", '"+g=Optimica"')
				self.__requestApi("loadFile", fName)
			else:
				raise Exception('loadFile Error: ' + loadfileError)
				
	
	def getModelicaPath(self):
		"""
		get the environment variable OPENMODELICALIBRARY
		"""
		return self.__requestApi("getModelicaPath")
		
	
	def setModelicaPath(self, path):
		"""
		set the environment variable OPENMODELICALIBRARY
		@return true if successful
		**not tested
		"""
		exp = 'setModelicaPath("{}")'.format(path)
		return self.__omc.sendExpression(exp)
	
			
	def getComponents2(self):
		"""
		to get list of all ModelicaVariable names
		"""
		return self.__requestApi("listVariables")
		#return self.__omc.getComponents(cName)
	
	
	def getComponents(self):
		"""
		to get list of all ModelicaVariable names
		Note: Model must be built before this function can be called!
		"""
		if not self.__buildFlag:
			raise Exception("Model must be built before getComponents() can be called!")
			
		components = []
		for k in self.__variablesList:
			components.append(k.name)
		
		return components
	
	
	def isModel(self, model):
		"""
		Returns True if the given class has restriction model.
		"""
		return self.__requestApi("isModel", str(model))
	
	
	def getPackages(self):
		"""
		Returns a list of names of libraries and their path on the system, for example:
		(("Modelica","/usr/lib/omlibrary/Modelica 3.2.1"),("ModelicaServices","/usr/lib/omlibrary/ModelicaServices 3.2.1"))
		"""
		return self.__requestApi("getLoadedLibraries")
	
	
	def buildModel(self, mName):
		"""
		builds a modelica model by generating c code and build it.
		The only required argument is the className, while all others have some default values.

		@param str mName:  Name of model, e.g. Modelica.Mechanics.Rotational.Components.Clutch.
		
		@input str model: the model that should simulated
		@input float startTime: the start time of the simulation. <default> = 0.0
		@input float stopTime: the stop time of the simulation. <default> = 1.0
		@input float numberOfIntervals: number of intervals in the result file. <default> = 500
		@input float tolerance: tolerance used by the integration method. <default> = 1e-6
		@input str method: integration method used for simulation. <default> = "dassl"
		@input str fileNamePrefix: fileNamePrefix. <default> = ""
		@input str options: options. <default> = ""
		@input str outputFormat: Format for the result file. <default> = "mat"
		@input str variableFilter: Filter for variables that should store in result file. <default> = .*

		@input str cflags: compiler flags. <default> = "" (more info: https://www.openmodelica.org/doc/OpenModelicaUsersGuide/latest/omchelptext.html)
		@input str simflags: simflags. <default> = "" (more info: https://www.openmodelica.org/doc/OpenModelicaUsersGuide/latest/simulationflags.html)
		
		@output String[2] buildModelResults;

		@raises: Exception()
		"""
		self.modelName=mName
		
		#check if the model exists
		if not self.isModel(mName):
			raise Exception('The model "{}" does not exists'.format(mName))
		
		#d: Shows additional information from the initialization process.
		self.__omc.sendExpression("setCommandLineOptions(\"+d=initialization\")")
		
		buildModelResult = self.__requestApi("buildModel", mName)

		#print warnings
		if self.__requestApi("countMessages")[2]:
			print("building warnings:")
		buildModelError = self.__requestApi("getErrorString")
		
		if ('' in buildModelResult):
			raise Exception(buildModelError)
	
		#create list self.__variablesList. It displays details of ModelicaVariables such as name, value,
		#changeable, and description, where changeable means if value for corresponding quantity name
		#is changeable or not.
		#Parse XML file:
		self.__xmlFile = os.path.join(self.changeWorkingDirectory(), buildModelResult[1])
		self.__xmlTree = ET.parse(self.__xmlFile)
		self.__xmlRoot = self.__xmlTree.getroot()
		
		for sv in self.__xmlRoot.iter('ScalarVariable'):
			name = sv.get('name')
			changable = sv.get('isValueChangeable')
			description = sv.get('description')
			variability = sv.get('variability')
			causality = sv.get('causality')
			alias = sv.get('alias')
			aliasvariable = sv.get('aliasVariable')
			ch = sv.getchildren()
			start = None
			for att in ch:
				start = att.get('start')
			self.__variablesList.append(ModelicaVariable(name, start, changable, variability, description, causality, alias, aliasvariable))
	
		self.__buildFlag=True
	
	
	def checkAvailability(self, names, chkList):
		"""
		check if names exist in a list
		"""
		try:
			if isinstance(names, list):
				nonExistingList = []
				for n in names:
					if n not in chkList:
						nonExistingList.append(n)
				if nonExistingList:
					print('Error!!! ' + str(nonExistingList) + ' does not exist.')
					return False
			elif isinstance(names, str):
				if names not in chkList:
					print('Error!!! ' + names + ' does not exist.')
					return False
			else:
				print('Error!!! Incorrect format')
				return False
			return True
	
		except Exception as e:
			print(e)
	
	
	def setSimulationOptions(self, **simOptions):
		"""
		change the simulation values 'startTime', 'stopTime', 'stepSize', 'tolerance', 'solver' in the xml model description file
		
		@input float startTime: the start time of the simulation.
		@input float stopTime: the stop time of the simulation.
		@input float stepSize: the step time of the simulation.
		@input float tolerance: tolerance used by the integration method.
		@input float solver: integration method
		
		Note: Model must be built before this function can be called!
		"""
		if not self.__buildFlag:
			raise Exception("Model must be built before setSimulationOptions() can be called!")
			
		valuesList = self.getSimulationValues()
		simNamesList = ['startTime', 'stopTime', 'stepSize', 'tolerance', 'solver']
		try:
			for key in simOptions:
				if key in simNamesList:
					if key == 'stopTime':
						if float(simOptions[key]) <= float(valuesList[0]):
							print('!!! stoptTime should be greater than startTime')
							return
					if key == 'startTime':
						if float(simOptions[key]) >= float(valuesList[1]):
							print('!!! startTime should be less than stopTime')
							return
					
					for sim in self.__xmlRoot.iter('DefaultExperiment'):
						sim.set(key, str(simOptions.get(key)))
						self.__xmlTree.write(self.__xmlFile, encoding='UTF-8', xml_declaration=True)
						
				else:
					print('!!!' + key + ' is not an option')
					continue
	
		except Exception as e:
			print(e)
	
	
	def getSimulationValues(self):
		"""
		Read the simulation values 'startTime', 'stopTime', 'stepSize', 'tolerance', 'solver' from the xml model description file 
		
		Note: Model must be built before this function can be called!
		"""
		if self.__xmlFile is None:
			raise Exception("Model must be built before setSimulationOptions() can be called!")
			
		simValuesList = []
		
		for attr in self.__xmlRoot.iter('DefaultExperiment'):
			startTime = attr.get('startTime')
			simValuesList.append(float(startTime))
			stopTime = attr.get('stopTime')
			simValuesList.append(float(stopTime))
			stepSize = attr.get('stepSize')
			simValuesList.append(float(stepSize))
			tolerance = attr.get('tolerance')
			simValuesList.append(float(tolerance))
			solver = attr.get('solver')
			simValuesList.append(solver)
			
		return simValuesList
	
	
	def setValue(self, values):
		"""
		change simulation variables in the xml model description file
		
		Note: Model must be built before this function can be called!
		"""
		if not self.__buildFlag:
			raise Exception("Model must be built before OMFunctions.setValue() can be called!")
		
		namesList=self.getComponents()
		for n in values:
			if n in namesList:
				for l in self.__variablesList:
					if (l.name == n):
						if l.changable == 'false':
							raise Exception('!!! value cannot be set for {}'.format(n))
						
						else:
							l.start = float(values.get(n))
							for paramVar in self.__xmlRoot.iter('ScalarVariable'):
								if paramVar.get('name') == str(n):
									c = paramVar.getchildren()
									for attr in c:
										val = float(values.get(n))
										attr.set('start', str(val))
										self.__xmlTree.write(self.__xmlFile, encoding='UTF-8', xml_declaration=True)
			else:
				raise Exception('Error: {} is not a variable'.format(n))
	
	
	def simulate(self, simFlags=None):
		"""
		to simulate or re-simulate model by calling the c programm according to the simulation options. 
		It can be called:
		•only without any arguments: simulate the model
		
		Note: Model must be built before this function can be called!
		"""
	
		if (platform.system() == "Windows"):
			exeFile = self.modelName + ".exe"
		else:
			exeFile = self.modelName
		
		exeFile = os.path.join(self.changeWorkingDirectory(), exeFile).replace("\\", "/")
		check_exeFile_ = os.path.exists(exeFile)
		
		if simFlags is not None:
			if not isinstance(simFlags, str):
				raise Exception('SimFlags must be a string')
			cmd = exeFile + ' {}'.format(simFlags)
		else:
			cmd = exeFile
			
			
		if (check_exeFile_):
			omhome = os.path.join(os.environ.get("OPENMODELICAHOME"), 'bin').replace("\\", "/")
			my_env = os.environ.copy()
			my_env["PATH"] = omhome + os.pathsep + my_env["PATH"]
			#process = subprocess.Popen(cmd, cwd=self.changeWorkingDirectory(), env=my_env)
			process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=self.changeWorkingDirectory(), env=my_env)
				
			while True:
				output = process.stdout.readline()
				output = output.decode('utf8')

				if output == '' and process.poll() is not None:
					break
				if output:
					if 'stdout            | info    | ' in output:
						sys.stdout.write('INFO: {}'.format(output.replace('stdout            | info    | ',"")))
						#print('INFO: {}'.format(output.replace('stdout            | info    | ',"")))
					elif 'stdout            | warning |' in output:
						sys.stdout.write('WARNING: {}'.format(output.replace('stdout            | warning | ',"")))
						#print('WARNING: {}'.format(output.replace('stdout            | warning | ',"")))
					else:
						sys.stdout.write(output)
						#print(output)
						
			rc = process.poll()
			process.terminate()
			
			if (rc is not 0):
				raise Exception(str(stderr))	#not tested
			
			self.__simulationFlag = True
			self.resultFileName = os.path.join(self.changeWorkingDirectory(), self.modelName + '_res.mat')
		
		else:
			print("Error: application file not generated yet")
	
	
	def simulateModel(self, model, **kwargs):
		"""
		Simulates a modelica model by generating c code, build it and run the simulation executable.
		The only required argument is the className, while all others have some default values.
		input str model: the model that should simulated
		input float startTime: the start time of the simulation. <default> = 0.0
		input float stopTime: the stop time of the simulation. <default> = 1.0
		input float numberOfIntervals: number of intervals in the result file. <default> = 500
		input float tolerance: tolerance used by the integration method. <default> = 1e-6
		input str method:integration method used for simulation. <default> = "dassl"
		input str fileNamePrefix: fileNamePrefix. <default> = ""
		input str options: options. <default> = ""
		input str outputFormat: Format for the result file. <default> = "mat"
		input str variableFilter: Filter for variables that should store in result file. <default> = .*

		input str cflags: compiler flags. <default> = "" (more info: https://www.openmodelica.org/doc/OpenModelicaUsersGuide/latest/omchelptext.html)
		input str simflags: simflags. <default> = "" (more info: https://www.openmodelica.org/doc/OpenModelicaUsersGuide/latest/simulationflags.html)
		output dict results={String resultFile, String simulationOptions, String messages, Real timeFrontend, Real timeBackend, 
			Real timeSimCode, Real timeTemplates, Real timeCompile, Real timeSimulation, Real timeTotal}
		
		Note: this function can not be used with the functions: simulate(), setValue(), getSimulationValues(), setSimulationOptions(), buildModel()
		"""

		command = "simulate(" + model 
		for k,v in kwargs.items():
			if isinstance(v, str):
				command += ', {}="{}"'.format(k, v)
			else: 
				#command += ", " + k + "=" + str(v)
				command += ', {}={}'.format(k, v)
		command+= ")"
		
		results = self.__omc.sendExpression(command)
		if not results:
			err_msg = self.__omc.sendExpression("getErrorString()")
			raise Exception(err_msg)

		self.resultFileName = os.path.join(self.changeWorkingDirectory(), results['resultFile'])
		self.__simulationFlag = True
		
		return results
	
	
	def getResultVarNames(self):
		""" Returns the variablen names from the simulation result."""
		# check for result file exits
		if not os.path.exists(self.resultFileName):
			raise Exception("Error: Result file does not exist.")
		
		validSolution = self.__omc.sendExpression("readSimulationResultVars(\"" + self.resultFileName + "\")")
		return validSolution
	
	
	# to extract simulation results
	def getResults(self, outputVarList = []):
		"""
		Returns the solutions for the variables in outputVarList in
		the format of numpy.array by using OMPython in the same order as outputVarList
		"""
		
		# check for result file exits
		if not os.path.exists(self.resultFileName):
			raise Exception("Error: Result file does not exist.")

		if all(isinstance(a, str) for a in outputVarList):
			outputList = self.getResultVarNames()
			for v in outputVarList:
				if v == 'time':
					continue
				if v not in outputList:
					raise Exception('!!! {} does not exist\n'.format(v))
									
			variables = ",".join(outputVarList)
			exp = "readSimulationResult(\"" + self.resultFileName + '", {' + variables + '})'
	        
			res = self.__omc.sendExpression(exp)
			exp2 = "closeSimulationResultFile()"
			self.__omc.sendExpression(exp2)
			
			return np.array(res)
	
	
	def getResultsFast(self, outputVarList=[]):
		"""
		Returns the trajectories for the variables in outputVarList in
		the format of numpy.array by using ModelicaRes
		
		This Method can work faster than getSolutions() if the number of solutions is big
		"""
		
		# check for result file exits
		if not os.path.exists(self.resultFileName):
			raise Exception("Error: Result file does not exist.")
		
		if all(isinstance(a, str) for a in outputVarList):
			outputList = self.getResultVarNames()
			for v in outputVarList:
				if v == 'time':
					continue
				if v not in outputList:
					raise Exception('!!! {} does not exist\n'.format(v))
					
			sim=SimRes(self.resultFileName)
			arrTraj=[]        
			for item in outputVarList:
				if np.array_equal(sim('Time').samples.values,sim(item).samples.times):
					arrTraj.append(sim(item).samples.values)
				else:
					arrTraj.append(np.array(np.interp(sim('Time').samples.values,sim(item).samples.times,sim(item).samples.values),'float32'))            
			return np.array(arrTraj)
	
	
	# request to OMC
	def __requestApi(self, apiName, entity=None, properties=None):
		if (entity is not None and properties is not None):
			exp = '{}({}, {})'.format(apiName, entity, properties)
		elif entity is not None and properties is None:
			if (apiName == "loadFile" or apiName == "importFMU"):
				exp = '{}("{}")'.format(apiName, entity)
			else:
				exp = '{}({})'.format(apiName, entity)
		else:
			exp = '{}()'.format(apiName)
		try:
			res = self.__omc.sendExpression(exp)
		except Exception as e:
			print(e)
			res = None
		return res
	
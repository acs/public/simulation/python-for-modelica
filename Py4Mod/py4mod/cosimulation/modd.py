from py4mod.modelicafunctions import ModelicaModel
import re
import os

def connectCompsToModd(model, compList, outputPerComp, InputPerComp, moddSystem, timeStep, tcpPort, mapFileNameInputs, mapFileNameOutputs):
    '''Connects components in compList to moddSystem in ModelicaModel model.
    '''
    InputsMap = ["Index;Variable\n"]
    OutputsMap = ["Index;Variable\n"]

    numModelInputs = len(compList) * InputPerComp
    numModelOutputs = len(compList) * outputPerComp
    model.changeVars([[moddSystem + ".n_Data_In", numModelInputs], [moddSystem + ".n_Data_Out", numModelOutputs],
                   [moddSystem + ".sample_period", timeStep],[moddSystem + ".tcp_port", tcpPort]])

    idxSend = 1
    idxRec = 1
    for comp in compList:
        if outputPerComp > 1:
            for m in range(outputPerComp):
                model.addConnection(comp +".Data_Out["+str(m+1)+"]", moddSystem + ".Send_Data["+str(idxSend)+"]")
                OutputsMap.append(str(idxSend) +";" + comp +".Data_Out["+str(m+1)+"]" + "\n")
                idxSend+=1
        else:
            model.addConnection(comp +".Data_Out", moddSystem + ".Send_Data["+str(idxSend)+"]")
            OutputsMap.append(str(idxSend) +";" + comp +".Data_Out" + "\n")
            idxSend+=1
        if InputPerComp > 1:
            for n in range(InputPerComp):
                model.addConnection(comp + ".Data_In["+str(n+1)+"]", moddSystem + ".Receive_Data["+str(idxRec)+"]")
                InputsMap.append(str(idxRec) +";" + comp + ".Data_In["+str(n+1)+"]" + "\n")
                idxRec += 1
        else:
            model.addConnection(comp + ".Data_In", moddSystem + ".Receive_Data["+str(idxRec)+"]")
            InputsMap.append(str(idxRec) +";" + comp + ".Data_In" + "\n")
            idxRec += 1

    mappingFile = open(mapFileNameInputs, "w")
    mappingFile.writelines(InputsMap)
    mappingFile.close()
    
    mappingFile = open(mapFileNameOutputs, "w")
    mappingFile.writelines(OutputsMap)
    mappingFile.close()

def connect(mo, system, n_list, interfaces, model, postfix, mappingFileName, mapGroup, pin1, pin2, tmpPin):
    index = 1
    if mapGroup:
        mapping = ["Index;Variable;Group\n"]
    else:
        mapping = ["Index;Variable\n"]

    for i in range(0, len(n_list)):
        connections = mo.getConnection(interfaces[i])

        for j in range(1, n_list[i] + 1):
            #gets the variable to wich the systemin/out is connected
           pin = connections[1][connections[0].index(interfaces[i]+"." + tmpPin + "["+ str(j) + "]")]

           #removes the postfix from the pin name
           regex = re.search(r"^([\w\.]+)" + postfix , pin)
           pin = regex.group(1)

           if mapGroup:
               group = mo.getValues([interfaces[i] + ".Group"])[0]
               mapping.append(str(j+index-1) +";" + pin + ";" + group + "\n")
           else:
               mapping.append(str(j+index-1) +";" + pin + "\n")

        model.addConnection(interfaces[i] + "." + pin2,  system + "." + pin1 + "[" + str(index) + ":" +  str(index+n_list[i] - 1) +"]")
        index = index + n_list[i]

    mappingFile = open(mappingFileName, "w")
    mappingFile.writelines(mapping)
    mappingFile.close()

def data(model, projectpath, configPath, i):
    sampleTime = 900
    port = 27017 + i

    configPath = os.path.join(configPath, "SimConfig", model)
    if not os.path.exists(configPath):
        os.makedirs(configPath)

    mo = ModelicaModel(model, projectpath)
    mo.copyModel("_connected")

    inputs = mo.getSubmodelNames("System_In")
    outputs = mo.getSubmodelNames("System_Out")

    n_Inputs_list = mo.getValues([item + ".n_Data" for item in inputs])
    n_Inputs = sum(n_Inputs_list)
    n_Outputs_list = mo.getValues([item + ".n_Data" for item in outputs])
    n_Outputs = sum(n_Outputs_list)

    mo.changeSubmodelType("ModPowerSystems.System", "CoSimulation.System_Data")
    system = mo.getSubmodelNames("CoSimulation.System_Data")[0]
    mo.changeVars([[system + ".n_Data_In", n_Inputs], [system + ".n_Data_Out", n_Outputs], [system + ".tCPIP_RecvClient_IO.sampleTime", sampleTime], [system + ".port", port], [system + ".tCPIP_SendClient_IO.sampleTime", sampleTime]])
    if(n_Inputs > 0):
        mo.changeVars([[system + ".n_Data_In", n_Inputs]])
        connect(mo, system, n_Inputs_list, inputs, mo, r"_[Ii]n", os.path.join(configPath, "mappingIn.csv"), False, "Receive_Data", "u", "y")

    if(n_Outputs > 0):
        mo.changeVars([[system + ".n_Data_Out", n_Outputs]])
        connect(mo, system, n_Outputs_list, outputs, mo, r"_[Oo]ut", os.path.join(configPath, "mappingOut.csv"), True, "Send_Data", "y", "u")

# Funktionennamen

The following table gives a comparison between the names of the functions of the interfaces and a small description of their function. It is important to keep in mind that some functions receive different arguments depending on the chosen interface.


| DymolaInterface          | OpenModelica Interface   | ModelicaModel          |                                                                                                        |
| ------------------------ | ------------------------ | -------------          | ------------------------------------------------------------------------------------------------------ |
| __init__                 | __init__                 | createInterface        | In OM creates an OMCSessionZMQ object, in DymInterface starts Dymola                                   |
| changeWorkingDirectory   | changeWorkingDirectory   | changeWorkingDirectory | Change directory to the given path                                                                     |
|  **                      | loadLibrary              |     ---                | Loads a Modelica library from the path indicated by the environment variable OPENMODELICALIBRARY       |
|  ---                     | getAvailableLibraries    |     ---                | Looks for all libraries that are visible from the getModelicaPath()                                    |
| loadFile                 | loadFile                 | loadFile               | Reads the file specified by fName. This corresponds to File->Open in the menus.                        |
|  ---                     | getModelicaPath          |     ---                | Returns the environment variable OPENMODELICALIBRARY                                                   |
|  ---                     | setModelicaPath          |     ---                | To set the environment variable OPENMODELICALIBRARY                                                    |
|  ---                     | getComponents            |     ---                | To get list of all ModelicaVariable names                                                              |
|  ---                     | isModel                  |     ---                | Returns True if the given class has restriction model.                                                 |
|  ---                     | getPackages              |     ---                | Returns a list of names of loaded libraries and their path on the system                               |
| dymApplyModifiers        | setValue                 |     ---                | In Dymola applies modifiers in modifierList. In OM chage the starts values in the XML init file        |
| buildModel               | buildModel               | buildModel             | Builds a modelica model by generating c code and build it                                              |
| simulateModel            | simulate                 | simulate               | In OM simulate or re-simulate model by calling the .exe file (generated with buildModel()). In Dymola simulates the model and compiles it if it has not been compiled before |
| ---                      | simulateModel            | ---                    | In OM simulates a modelica model by generating c code, build it and run the simulation executable.     |
| getResultVarNames        | getResultVarNames        | getResultVarNames      | Returns the variablen names from the simulation result                                                 |
| getResultsFast           | getResultsFast           | getResults             | Returns the trajectories for the variables in outputVarList by using ModelicaRes                       |
| getResults               | getResults               |     ---                | To extract simulation results by using OMPython/DymolaInterface                                        |
| close                    | close                    | close                  | To close dymola or OM                                                                                  |

<br />
** This function is not necesary in Dymola <br />
--- This function is not implemented in the Interface 

<br />
The functions that have not been implemented in ModelicaModel but in any of the interfaces can be easily called with the class variable self.interface. 

# -*- coding: latin -*-

from shutil import copyfile
from shutil import copy
from bisect import bisect
import numpy as np
import re
import ast
# moved interface imports for Dymola and OM to create Interface to allow for OMInterface use with Python 3.x

class ModelicaModel(object):
	def __init__(self, model, mainPath):
		"""Constructor of the class ModelicaModel, reads and format a model that
		is given by the path of the project and the Name it has in this Project
		"""
		self._model = model # model identifier in package hierarchy [e.g. packageName.subPackageName.modelName]
		self._mainPath = mainPath # path where either the model file itself or the corresponding package.mo can be found
		if not self._mainPath[-1] == "/":
			self._mainPath = self._mainPath + "/"
		self._modelName = self.getModelName() # actual name of model extracted from self._model
		self._modelPath = self.__getModelPath() # complete path of the model file deduced from self._mainPath and self._model			  
		self._modelLines = self.__readModel()
		self._formatModel()
		self._values = {}
		self.interface = None				#interface object
		self.interaceName = ""				#"DYMOLA" or "OPENMODELICA"

		
	#-------------------------- FUNCTIONS TO USE DYMOLA OR OPENMODELICA -------------------------#
	def createInterface(self, interface, **kwargs):
		if interface=='DYMOLA':
			from .DymInterface import DymInterface
			
			dymola_path = ""	
			use64bit = True
			port = -1
			showwindow  = False
			debug = False
			allowremote = False
			nolibraryscripts = False
		
			for key, value in kwargs.items():
				if key=='dymola_path':
					dymola_path=value
				elif key=='use64bit':
					use64bit=value
				elif key=='port':
					port=value
				elif key=='showwindow':
					showwindow=value
				elif key=='debug':
					debug=value
				elif key=='allowremote':
					allowremote=value
				elif key=='nolibraryscripts':
					nolibraryscripts=value
						
			self.interface = DymInterface(dymola_path, use64bit, port, showwindow, debug, allowremote, nolibraryscripts)
			self.interfaceName=interface

		elif interface=='OPENMODELICA':
			from .OMInterface import OMInterface
			self.interface = OMInterface()
			for key,value in kwargs.items():
				if key=='om_path':
					self.interface.setModelicaPath(value)
			self.interfaceName=interface
		
		else:
			raise Exception('The possible Interfaces are "{}" and "{}"'.format('DYMOLA', 'OPENMODELICA'))
	
	def changeWorkingDirectory(self, dir=""):
		"""
		Change directory to the given path
		If the given path is the empty string, the function simply returns the current working directory
		
		@param str Dir: Directory to change to. If directory="" this function return the current directory
		@raises: exception
		"""
		if self.interface is None:
			raise Exception('The interface has not yet been initialized !!!')
		
		if dir=="": 
			return self.interface.changeWorkingDirectory(dir)		
		
		self.interface.changeWorkingDirectory(dir)	
		
	def loadFile(self, fName):
		"""
		Reads the file specified by fName.
		This corresponds to File->Open in the menus. 
		
		@param str fName: File-path to open.
		@raises: Exception()
		"""
		if self.interface is None:
			raise Exception('The interface has not yet been initialized !!!')
		
		self.interface.loadFile(fName)			
		
	def buildModel(self, **kwargs):
		"""
		builds self._model by generating c code and build it.

		other arguments for modelica interface:
		@input float startTime: the start time of the simulation. <default> = 0.0
		@input float stopTime: the stop time of the simulation. <default> = 1.0
		@input float numberOfIntervals: number of intervals in the result file. <default> = 500
		@input float tolerance: tolerance used by the integration method. <default> = 1e-6
		@input str method: integration method used for simulation. <default> = "dassl"
		@input str fileNamePrefix: fileNamePrefix. <default> = ""
		@input str options: options. <default> = ""
		@input str outputFormat: Format for the result file. <default> = "mat"
		@input str variableFilter: Filter for variables that should store in result file. <default> = .*
		@input str cflags: compiler flags. <default> = "" (more info: https://www.openmodelica.org/doc/OpenModelicaUsersGuide/latest/omchelptext.html)
		@input str simflags: simflags. <default> = "" (more info: https://www.openmodelica.org/doc/OpenModelicaUsersGuide/latest/simulationflags.html)
		"""
		if self.interfaceName=='OPENMODELICA':
			self.interface.buildModel(self._model, **kwargs)
		
		elif self.interfaceName=='DYMOLA':
			self.interface.buildModel(self._model)				
		
		elif self.interface is None:
			raise Exception('The interface has not yet been initialized !!!')

	def simulate(self, **kwargs):
		"""
		DYMOLA: Simulates self._model 
		optional arguments for the dymola interface:
		@param float startTime: Start of simulation.
		@param float stopTime: End of simulation.
		@param int numberOfIntervals: Number of output points.
		@param float outputInterval: Distance between output points.
		@param str method: Integration method.
		@param float tolerance: Tolerance of integration.
		@param float fixedstepsize: Fixed step size for Euler.
		@param str resultFile: Where to store result.
		@param str[] initialNames: Parameters and start-values to set. Dimension ``[:]
		
		MODELICA: simulates or re-simulate the model that was compiled with ModelicaModel.buildModel()
		 - optional arguments for the modelica interface
		 @input str simFlags: simflags. <default> = "" (more info: https://www.openmodelica.org/doc/OpenModelicaUsersGuide/latest/simulationflags.html)
		Note: Model must be built before this function can be called!
		"""
		
		if self.interfaceName=='DYMOLA':
			startTime=0
			stopTime=10
			numberOfIntervals=0
			outputInterval=1
			method='Dassl'
			tolerance=0.0001 
			fixedstepsize=0.0
			resultFile="dsres"
			initialNames=[]
			initialValues=[] 
			finalNames=[]
			autoLoad=False
			
			for key, value in kwargs.items():
				if key=='startTime':
					startTime=value
				elif key=='stopTime':
					stopTime=value	
				elif key=='numberOfIntervals':
					numberOfIntervals=value	
				elif key=='outputInterval':
					outputInterval=value	
				elif key=='method':
					method=value	
				elif key=='tolerance':
					tolerance=value	
				elif key=='fixedstepsize':
					fixedstepsize=value	
				elif key=='resultFile':
					resultFile=value
				elif key=='initialNames':
					initialNames=value
				elif key=='initialValues':
					initialValues=value
				elif key=='finalNames':
					finalNames=value
				elif key=='autoLoad':
					autoLoad=value
					
			self.interface.simulateModel(self._model, startTime, stopTime, 	numberOfIntervals, 
						outputInterval, method, tolerance, fixedstepsize, resultFile, 
						initialNames, initialValues, finalNames, autoLoad)
	
		elif self.interfaceName=='OPENMODELICA':
			simflags=None
			for key, value in kwargs.items():
				if key=='simflags':
					simflags=value
									
			self.interface.simulate(simflags)
		
		elif self.interface is None:
			raise Exception('The interface has not yet been initialized !!!')

	def getResults(self, outputVarList = []):
		if self.interface is None:
			raise Exception('The interface has not yet been initialized !!!')
			
		return self.interface.getResultsFast(outputVarList)
			
	def getResultVarNames(self):
		if self.interface is None:
			raise Exception('The interface has not yet been initialized !!!')
		
		return self.interface.getResultVarNames()
			
	def close(self):
		"""
		Terminate execution of the interface
		"""
		if self.interfaceName=='OPENMODELICA':
			self.interface.buildModel(self._model, **kwargs)
		
		self.interface.close()
		self.interface=None


	#-------------------------- FUNCTIONS TO WORK ON MODELICA CODE ------------------------------#
	def getSubmodelNames(self, subModelType):
		"""Searches every instance of the type subModelType and returns a list with
		the names they have in the model.
		"""
		subModelNames = []
		for i in range(self.__var_start, self.__var_end +1):
			mo = re.search(subModelType + r" +(\w+)[ (]?", self._modelLines[i])
			if mo:
				name = mo.group(1)
				subModelNames.append(name)
		return subModelNames
		
	def getSubmodelModifierValue(self,listSubmodelNames,modifierName):
		modValue = []
		positions=[]
		for submodelName in listSubmodelNames:
			positions.append(self.__getLinePositionName(submodelName))
		for pos in positions:
			mo = re.search(modifierName + r"=(\d+)", self._modelLines[pos])
			if mo:
				val = mo.group(1)
				modValue.append(val)
		return modValue
		 
	def getSubModelTypesAll(self):
		""" Returns a list of all submodeltypes that the model contains
		"""
		submodeltypes= []
		for i in range(self.__var_start,self.__var_end):
			mo = re.search(r" *(?:inner)? ([\w\.]+) +(?:\w+)[ (]?", self._modelLines[i])
			if mo:
				submodeltype = mo.group(1)
				if not (submodeltype in submodeltypes):
					submodeltypes.append(submodeltype)
		return submodeltypes
		
	
	def changeSubmodelTypeAll(self, oldType, newType, positions = []):
		"""Changes the type of all submodels with the type oldType to the type newType.
		ATTENTION: this can lead to Problems if the equations (e.g. connections) don't
		match with the new type.
		"""
		if positions == []:		
			positions = self.__getLinePositionSubmodel(oldType)
		for i in positions:
			mo = re.search(oldType + r" ", self._modelLines[i])
			start = mo.start()
			end = mo.end() - 1
			self._modelLines[i] = self._modelLines[i][:start] + newType + self._modelLines[i][end:] 
		self.__writeModel()
		
	def changeSubmodelType(self, listSubmodelNames, oldType, newType):
		"""Changes the type of all submodels in listSubmodelNames from oldType to type newType.
		ATTENTION: this can lead to Problems if the equations (e.g. connections) don't
		match with the new type.
		"""
		positions=[]
		for submodelName in listSubmodelNames:
			positions.append(self.__getLinePositionName(submodelName))
		for pos in positions:
			mo = re.search(oldType + r" ", self._modelLines[pos])
			start = mo.start()
			end = mo.end() - 1
			self._modelLines[pos] = self._modelLines[pos][:start] + newType + self._modelLines[pos][end:] 
		self.__writeModel()   
		
	def changeSubmodelName(self, dictSubmodelNames):
		"""Changes the names of the submodels in the declaration according to dictSubmodelNames, which includes the 
		renaming entries in the manner {"oldSubmodelName":"newSubmodelName"}
		ATTENTION: corresponding connections, which include the old submodel name, must be changed seperatedly by using changeConnection
		or changeConnectionPin
		"""		
		for submodelNameOld,submodelNameNew in dictSubmodelNames.iteritems():
			pos = self.__getLinePositionName(submodelNameOld)
			self._modelLines[pos] = re.sub(submodelNameOld,submodelNameNew,self._modelLines[pos])
		self.__writeModel()
		
	def changeVars(self, varList, overwrite = True):
		"""Changes or adds the initial values of variables/parameters that are specified
		in the variable-section of the model.
		The list varList contains the variables and the values that should be set.
		The items stored in varList have the form of ["Submodel.Subsubmodel", value]
		if you set the value of a differential variable the variable name has to end
		with ".start".
		E.g. if you want to change the startvalue of zLoad.v.re to 4, varList 
		has to contain the item ["zLoad.v.re.start", 4].
		If the overwrite flag is set to False, existing Values won't be changed.
		"""	   
		for item in varList:
			name = item[0]
			mo = re.search(r"^(\w+)\.", name)
			mainName = mo.group(1)
			pos = self.__getLinePositionName(mainName)
			line = self._modelLines[pos]
			self.__addLinetoValues(mainName)
			self.__addtoValues(item)
			#mo = re.search(r"^(\s+\w* *\w[\w\.]+\w) ", line)
			mo = re.search(r"^(\s*\w* *\w[\w\.]+\w) ", line)
			prefix =  mo.group(1)
			line = prefix + " " + self.__writeVariableString(self._values[mainName], mainName) + "\n"
			self._modelLines[pos] = line

		self.__writeModel()
		
	def getValues(self, varList):
		"""Returns a list of the current initial values of the variables and parameters given in varList."""
		values = []
		for item in varList:
			name = item
			mo = re.search(r"^(\w+)\.", name)
			mainName = mo.group(1)			
			self.__addLinetoValues(mainName)
			value = self.__searchinValues(item)
			values.append(value)
		return values
					 
	def deleteValues(self, varList):
		"""Deletes the initial values for variables or parameters given in varList.
		You can delete single values or a whole group of values.
		E.g if you want to delete the value for zLoad.v.re and zLoad.v.im
		var list can be ["zLoad.v.re", "zLoad.v.im"] or ["zLoad.v"].
		"""
		for item in varList:
			name = item
			mo = re.search(r"^(\w+)\.", name)
			mainName = mo.group(1)
			pos = self.__getLinePositionName(mainName)
			line = self._modelLines[pos]
			self.__addLinetoValues(mainName)
			self.__deletefromValues(self._values, item.split("."))
			#mo = re.search(r"^(\s+\w* *\w[\w\.]+\w) ", line)
			mo = re.search(r"^(\s*\w* *\w[\w\.]+\w) ", line)
			prefix =  mo.group(1)

			if mainName in self._values.keys():				
				line = prefix + " " + self.__writeVariableString(self._values[mainName], mainName) + "\n"
			else: 
				line = prefix + " " + mainName + "\n"
			self._modelLines[pos] = line
		self.__writeModel()		  

	def addConnection(self, pin1, pin2):
		"""Creates a new connection between pin1 and pin2 in the equation-section."""
		line = "  connect(" + pin1 + "," + pin2 + ");\n"
		self._modelLines.insert(self.__eq_end, line)
		self.__eq_end = self.__eq_end + 1
		self.__writeModel()
		
	def getConnection(self, name):
		"""Returns the pins of connections which contain name.
		Name can be either just a Submodel so that this function returns all connection that
		exist for this Submodel specific connector.
		The function returns two lists, the first list contains the pins that conatins name and the
		second one contains the pin that are part of the connections.
		"""
		pin1 = []
		pin2 = []
		for i in range(self.__eq_start, self.__eq_end):
			line = self._modelLines[i]
			if "connect" in line and name in line:
				mo = re.search(r"connect\( ?([\w\.\[\]]+) ?, ?([\w\.\[\]]+) ?\)", line)
				if name in mo.group(1):
					pin1.append(mo.group(1))
					pin2.append(mo.group(2))
				else:
					pin1.append(mo.group(2))
					pin2.append(mo.group(1))
		return [pin1, pin2]
		
	def changeConnection(self, listConnectionPins):
		"""
		Changes the definition of a specific connection allowing to redefine both pins
		The items stored in listConnectionPins must be in the form of ["oldConnectionPin1","oldConnectionPin2","newConnectionPin1","newConnectionPin2"]
		"""		
		for i in range(self.__eq_start, self.__eq_end):
			line = self._modelLines[i]
			if "connect" in line and listConnectionPins[0] in line and listConnectionPins[1] in line:
				line=re.sub(listConnectionPins[0],listConnectionPins[2],line)
				self._modelLines[i]=re.sub(listConnectionPins[1],listConnectionPins[3],line)
		self.__writeModel()
		
	def changeConnectionPin(self, listConnectionPin):
		"""
		Substitutes all the appearances of a specific connection pin by a new one
		listConnectionPin should be in the form of ["oldConnectionPin","newConnectionPin"]
		"""		
		for i in range(self.__eq_start, self.__eq_end):
			line = self._modelLines[i]
			if "connect" in line and listConnectionPin[0] in line:
				self._modelLines[i]=re.sub(listConnectionPin[0],listConnectionPin[1],line)
		self.__writeModel()		
	
	def copyModelToDir(self, destDir="", postfix=""):
		"""Copys the model to the given directory and with the given postfix. At the destination directory no package integration is done.		
		The newly created model and its references are now stored in this class.
		"""
		
		oldModelPath = self._modelPath
		
		self._mainPath=destDir		
		self._model=self._modelName + postfix # incorporates that the model is extracted from any package				
		self._modelPath = self.__getModelPath()
		self._modelName = self.getModelName()
		
		copyfile(oldModelPath, self._modelPath) 
		
		if self._modelLines[0][0:6] == "within": # remove package integration				 
			self._modelLines[0]="within ;\n"
			
		self._modelLines[1] = "model " + self._modelName + "\n"
		self._modelLines[-1] = "end " + self._modelName + ";"
		
		self.__writeModel()
		
	   
	def copyModelIntoPackage(self, newModelName):
		"""Copys the model and gives it a new name.		
		The newly created model is integrated in the original package and is now the one that is stored in this class.
		"""		
		# Sets a new model name and creates the corresponding newly named file
		mo = re.search(r"([\w\.]+\.)\w+$", self._model)
		if not mo==None:
			self._model = mo.group(1) + newModelName 
		else:
			self._model = newModelName
		self._modelName = self.getModelName()		

		oldModelPath = self._modelPath		
		self._modelPath = self.__getModelPath()
		copyfile(oldModelPath, self._modelPath)
		
		self._modelLines[1] = "model " + self._modelName + "\n"
		self._modelLines[-1] = "end " + self._modelName + ";"		
		self.__writeModel()								
			
		# Integrate newly create model in the package
		packagePath = self._modelPath[:-(len(self._modelName + ".mo"))] + "package.order"
		package = open(packagePath, "r")
		packageData = [line for line in package]
		package.close()
		
		if not(self._modelName + "\n"  in packageData):
			i = bisect(packageData, self._modelName + "\n")
			packageData.insert(i, self._modelName + "\n")
			
		package = open(packagePath, "w")
		package.writelines(packageData)
		package.close()

		
				
	#--------------------------HELPER-FUNCTIONS-------------------------------#

	def __getModelPath(self):
		"""Creates the modelpath out of the projectpath and the model."""
		modelpath = re.sub(r"\.", "/", self._model)
		modelpath = self._mainPath + modelpath + ".mo"
		return  modelpath

	def getModelName(self):
		"""Extracts the modelname out of the name that the model has in 
		the project.
		e.g.:
		When the model is "ACS_PowerSystems.SinglePhase.Examples.StatPh_Examples.Line_PQLoad"
		it will return "Line_PQLoad"
		"""		
		mo = re.search(r"\.(\w+)$", self._model)
		if not mo==None:
			modelName = mo.group(1)
		else:
			modelName = self._model
		return modelName
		
	def __readModel(self):
		"""Reads the lines in the modelfile into a list"""
		modelFile = open(self.__getModelPath(), "r")
		modelLines = [line for line in modelFile]
		modelFile.close()
		return modelLines
	
	def __writeModel(self):
		"""Writes the lines stored in the private list modelLines back into the file"""
		modelFile = open(self._modelPath, "w")
		modelFile.writelines(self._modelLines)
		modelFile.close()  
	
	def _formatModel(self):
		"""Reformats the sourcecode of the model in a way so that it is easier for the script to work on it."""
		annotation = False
		annotation_next = False
		mergelines = False
		
		model_end = len(self._modelLines)-1
		self.__var_start = 0
		self.__var_end = 0
		self.__eq_start = 0
		self.__eq_end = 0	
		
		i = 0
		while i <= model_end :
			if self._modelLines[i][0:5] == "model":
				self.__var_start = i+1
				i = i + 1
				continue
			elif self._modelLines[i][0:8] == "equation":
				self.__var_end = i
				self.__eq_start = i + 1
				i = i + 1
				continue
			elif self._modelLines[i][0:3] == "end":
				self.__eq_end = i
				i = i + 1
				continue			
				
			if "annotation" in self._modelLines[i]:
				ann_start = self._modelLines[i].find("annotation")
				if not self._modelLines[i][:ann_start].lstrip() == "":
					self._modelLines.insert(i+1, "   " + self._modelLines[i][ann_start:])
					self._modelLines[i] = self._modelLines[i][:ann_start] + "\n"
					model_end = model_end + 1
					annotation_next = True
				else:
					annotation = True
					mergelines = False
					
			if not annotation and not self._modelLines[i] == "\n":
				if self._modelLines[i+1].lstrip()[0:10] == "annotation":
					annotation_next = True
				if mergelines == True:
					self._modelLines[i-1] = self._modelLines[i-1][:-1] + self._modelLines[i].lstrip()
					del self._modelLines[i]
					i = i - 1
					model_end = model_end - 1
					mergelines = False
				if  not (self._modelLines[i][-2:] == ";\n") and not(annotation_next):
					mergelines = True
			elif annotation:
				if self._modelLines[i][-2:] == ";\n":
					annotation = False
			i = i + 1
			if annotation_next == True:
				annotation_next = False
				annotation = True
				mergelines = False
		self.__writeModel()
		
	def __readVariableString(self, varStr):
		"""Transforms an initialization string into a dictionary.
		E.g. (Vnom(displayUnit="kV")=20e3,v(re(start=1),im(start=2)), i(re(start=3), im(start=4)))
		becomes {"Vnom":{"displayUnit":"kV","value":20e3},"v":{"re":{"start":1},"im":{"start":2}},"i":{"re":{"start":3},"im":{"start":4}}}.
		"""
		varStr = re.sub(r"\s", "", varStr)		
		varStr = re.sub(r"\)=(\d*(\.\d+)?(e\d+)?)",r",value=\1)",varStr)
		varStr = re.sub(r"=", "\":", varStr)
		varStr = re.sub(r"\(", "\":{\"", varStr)
		varStr = re.sub(r"\)", "}", varStr)
		varStr = re.sub(r",", ",\"", varStr)
		
		#marks the string values that aren't enclosed by qutoes in modelicacode e.g. "smoothnessSetting":Modelica.Blocks.Types.Smoothness.ConstantSegments
		blocklist = re.findall(r":([\w]+\.[\w\.]+)}",varStr)
		for item in blocklist:
			tmpStr = item
			varStr = re.sub(re.escape(tmpStr), "\"!" + tmpStr + "!\"", varStr)
		
		varStr = varStr[2:]
				
		dic = ast.literal_eval(varStr)
		
		return dic
				
	def __writeVariableString(self, dic, name):
		"""Recursive function that turns a dictionary into a string that can be used 
		to initialize a variable.
		"""
		varStr = name + "("
		for key in dic.keys():
			if type(dic[key]) == dict:
				tmpStr = self.__writeVariableString(dic[key], key)
			elif type(dic[key]) == str:
				#checks if the string should have quotes or not
				mo = re.search(r"!(.+)!", dic[key])
				if mo is None:
					tmpStr = key + " = \""+ str(dic[key]) + "\""
				else:
					tmpStr = key + " = " + str(mo.group(1))
			else:
				tmpStr = key + " = " + str(dic[key])
			varStr = varStr + " " + tmpStr + ","
		varStr = varStr[:-1] + ")"
		return varStr
		 
	def __getLinePositionName(self, name):
		"""Returns the linenumber of the line in which the  variable with the name 
		"name" are declared.
		"""
		for i in range(self.__var_start,self.__var_end):
			#mo = re.search( r" +(\w+) *\(", self._modelLines[i])
			#mo = re.search( r" +(\w+) *?(\([\w,\(\)=\"\. ]*\))?\s+$", self._modelLines[i])
			#mo = re.search( r" +(\w+) *?(\([\w,\(\)=\"\. -]*\))?\s+$", self._modelLines[i])						
			mo = re.search(r" *([\w\.]*) " + name + r" *?(\([\w,\(\)=\"\. -_]*\))?\s+", self._modelLines[i])				   
			if mo:
				break
		return i
 
	def __getLinePositionSubmodel(self, subModelType):
		"""Returns a list which contains the linenumbers of lines in which 
		variables with the type subModelType are declared.
		"""
		lines = []
		for i in range(self.__var_start,self.__var_end):
			mo = re.search(subModelType + r" +(\w+)[ (]?", self._modelLines[i])
			if mo:
				lines.append(i)
		return lines	   
			
	def __addtoValues(self, item):
		"""Adds a item with the form ["Submodel.Subsubmodel", value]. To the 
		dictionary self._values in which all initial values are stored.
		"""
		name = item[0]
		value = item[1]
		l = name.split(".")
		dic = self._values[l[0]]
		for i in range(1, len(l)-1):
			if not dic.has_key(l[i]):
				dic[l[i]] = {}
			dic = dic[l[i]]
		dic[l[-1]] = value
		
	def __deletefromValues(self, dic, item):
		"""Deletes an entry from a nested dictionary."""
		key = item[0]
		if not len(item) == 1:
			self.__deletefromValues(dic[key], item[1:])
			if dic[key] == {}:
				del dic[key]
		else:
			del dic[key]
		
	def __searchinValues(self, name):
		"""Searches a Variable in the dictionary self._values and returns the initial value of this variable."""
		l = name.split(".")
		dic = self._values[l[0]]
		for i in range(1, len(l)-1):
			if not dic.has_key(l[i]):
				dic[l[i]] = {}
			dic = dic[l[i]]
		value = dic[l[-1]]
		return value
		
	def __addLinetoValues(self, name):
		"""Adds the initial values of the variable withe the name "name" given 
		in the modelfile to self._modelLines.
		"""
		pos = self.__getLinePositionName(name)
		line = self._modelLines[pos]
		if not name in self._values:
			#mo = re.search(r" +(\w+(\([\w,\(\)=\"\. ]*\))?)\s+$", line)			
			#mo = re.search(r" *([\w\.]*) " + name + r" *?(\([\w,\(\)=\"\. -_]*\))?\s+", line)
			#mo = re.search(r" +(\w+(\([\w,\(\)=\"\. _]*\))?)(\s+\"\w+\")?\s+$", line)
			mo = re.search(r" +(\w+(\([\w,\(\)=\"\.\: _\\/-]*\))?)(\s+\"\w+\")?\s+$", line)
			varstr = mo.group(1)[len(name):]	  
			
			if not varstr == "":
				tmpDic = self.__readVariableString(varstr)
			else:
				tmpDic = {}
			self._values[name] = tmpDic
			
		

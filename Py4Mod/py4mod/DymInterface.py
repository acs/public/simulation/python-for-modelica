import os
import re
from dymola.dymola_interface import DymolaInterface
from dymola.dymola_exception import DymolaException
from modelicares import SimRes
import numpy as np

class DymInterface(object):
	def __init__(self, dymola_path = "", use64bit = True, port = -1, showwindow  = False, debug = False, allowremote = False, nolibraryscripts = False):
		"""
		Creates a wrapper for Dymola and starts Dymola.
		
		@param str dymolapath: The path to ``Dymola.exe``. Default is an empty string, which means that the path should be auto-detected.
		@param bool use64bit: If ``True``, the 64-bit Dymola is used. If ``False``, the 32-bit Dymola is used. Default is ``True``.
		@param int port: The port number to use for connecting to Dymola. Default is ``-1``, which means that a port should be auto-assigned.
		@param bool showwindow: This flag controls whether the Dymola window will be visible or not when calling functions in the interface. The default value is ``False``, which means that Dymola will be hidden.
		@param bool debug: For debugging the Dymola Python interface. Note that it is intended for debugging the interface, not Modelica models. Setting this variable to ``True`` outputs debug messages in the console. For advanced users only.
		@param bool allowremote: By default, Dymola only allows local connections. The default value is ``False``. Set this flag to ``True`` to allow access from other machines.
		@param bool nolibraryscripts: Corresponds to the command-line argument with the same name. The default value is ``False``.
		@raises: :class:DymolaException <dymola.dymola_exception.DymolaException>
		"""
		#dymola parameters
		self.__dymola = None				#dymola wrapper object
		self.__dymola_path = dymola_path	
		self.__use64bit = use64bit
		self.__port = port
		self.__showwindow  = showwindow
		self.__debug = debug
		self.__allowremote = allowremote
		self.__nolibraryscripts = nolibraryscripts
		
		#model variables
		self.modelName=""
		self.resultFileName=""
		                             
		try:
			#Instantiate the Dymola interface and starts Dymola   
			self.__dymola = DymolaInterface(self.__dymola_path, self.__use64bit, self.__port, self.__showwindow, self.__debug, self.__allowremote, self.__nolibraryscripts)

			# Set Dymola options                    
			self.__dymola.ExecuteCommand("experimentSetupOutput(events=false);")
			self.__dymola.ExecuteCommand("Advanced.OutputModelicaCode:=true;")
			self.__dymola.ExecuteCommand("Advanced.OutputModelicaCodeWithAliasVariables = true;")
			#dymola.ExecuteCommand("Advanced.StoreProtectedVariables:=true;")
			
		except DymolaException as ex:
			raise Exception(ex)
	
	
	def changeWorkingDirectory(self, dir=""):
		"""
		Change directory to the given path
		If the given path is the empty string, the function simply returns the current working directory
		
		@param str Dir: Directory to change to. If directory="" this function return the current directory
		@raises: exception
		"""
		if dir=="":
			test = self.__dymola.cd()
			cwd=self.__dymola.getLastError().replace(' ',"").replace('=true', "").replace('\n',"")
			return cwd
		
		if not os.path.exists(dir):
			raise Except("File Error: " + os.path.abspath(fileName) + " does not exists!!!")
				
		self.__dymola.cd(dir)
	
		
	def loadFile(self, fName, mustRead=True, changeDirectory=False):
		"""
		Reads the file specified by fName.
		This corresponds to File->Open in the menus. 
		
		@param str fName: File-path to open.
		@param bool mustRead=False means that if the file already is opened/read, it is not opened/read again. 
			If mustRead=true in such a case the user is promted for removing the present one and open it again. 
			The value false can be useful in scriping, when only wanting to assure that a certain file has been read.
		@param bool changeDirectory: If true we automatically change to the directory of the file.
		@returns: true if successful
		@raises: :class:DymolaException <dymola.dymola_exception.DymolaException>
		"""
		if not os.path.isfile(fName):
			msg = "File Error: " + os.path.abspath(fileName) + " does not exists!!!"
			raise Exception(msg)
		if not self.__dymola.openModel(fName, mustRead, changeDirectory):
			return False
		return True
			

	def dymApplyModifiers(self, modifierList):
		""" Applies modifiers in modifierList, which is of format
			modifierList=[['submodule1','param1','value1'],['submodule2','param2','value2'],...]        
		"""
		#function not tested 
		cmdString=""
		for i in range(len(modifierList)):
			cmdString+= modifierList[i][0] + "." + modifierList[i][1] + "=" + str(modifierList[i][2]) + ";"            
		self.__dymola.ExecuteCommand(cmdString) 
	
	
	def buildModel(self, model):
		"""
		Compile the model (with current settings). 
		This corresponds to Translate in the menus
		
		@param str problem: Name of model, e.g. Modelica.Mechanics.Rotational.Components.Clutch.
		@raises: Exception
	
		"""
		self.modelName=model
		try:
			if not self.__dymola.translateModel(model):
				msg = 'Translation failed. Below is the Translation log: \n' + self.__dymola.getLastError()
				raise Exception(msg)
		except DymolaException as err:
			msg = 'Error in dymTranslate: {}'.format(err)
			raise Exception(msg)
	
	
	def simulateModel(self, model, startTime=0, stopTime=10, 
						numberOfIntervals=0, outputInterval=1, method='Dassl', tolerance=0.0001, 
						fixedstepsize=0.0, resultFile="dsres", initialNames=[], initialValues=[], 
						finalNames=[], autoLoad=False):
		"""Simulates the model in the mode specified in simMode. The results are stored in 
        files with the name resultName. It returns a list of the Variable Names andanother
        list with the values they had at the start of the simulation.
        
		@param str model: Name of model, e.g. Modelica.Mechanics.Rotational.Components.Clutch.
		@param float startTime: Start of simulation.
		@param float stopTime: End of simulation.
		@param int numberOfIntervals: Number of output points.
		@param float outputInterval: Distance between output points.
		@param str method: Integration method.
		@param float tolerance: Tolerance of integration.
		@param float fixedstepsize: Fixed step size for Euler.
		@param str resultFile: Where to store result.
		@param str[] initialNames: Parameters and start-values to set. Dimension ``[:]
		"""
		try:				
			# starts simulation
			result = self.__dymola.simulateExtendedModel(model, startTime, stopTime, 
						numberOfIntervals,  outputInterval, method, tolerance, 
						fixedstepsize, resultFile, initialNames, initialValues, 
						finalNames, autoLoad)
			self.resultFileName=resultFile
						
			if not result:
				err_msg = "Simulation failed. Below is the simulation log:\n" + self.__dymola.getLastError()
				raise Exception(err_msg)
				
		except DymolaException as err:
			err_msg = str("Dymola failed. Below is the log:\n" + str(err))
			raise Exception(err_msg)
		
			
	def getResultsFast(self, outputVarList=[]):
		""" Returns the trajectories for the variables in outputVarList in
		the format of numpy.array by using ModelicaRes
		
		This Method can work faster than getSolutions() if the number of solutions is big
        """
		file = os.path.join(self.changeWorkingDirectory(""), self.resultFileName + ".mat")
		sim=SimRes(file)
		arrTraj=[]        
		for item in outputVarList:
			if np.array_equal(sim('Time').samples.values,sim(item).samples.times):
				arrTraj.append(sim(item).samples.values)
			else:
				arrTraj.append(np.array(np.interp(sim('Time').samples.values,sim(item).samples.times,sim(item).samples.values),'float32'))            
		return np.array(arrTraj)
	
	
	def getResults(self, outputVarList = []):
		""" Returns the trajectories for the variables in outputVarList in
		the format of numpy.array by using DymolaInterface in the same order as outputVarList
		"""
		sizeTraj = self.__dymola.readTrajectorySize(self.resultFileName + ".mat")
		arrTraj = np.array(self.__dymola.readTrajectory(self.resultFileName + ".mat", outputVarList, sizeTraj))    
		return arrTraj
	
	
	def getResultVarNames(self):
		""" Returns the variablen names from the simulation result."""
		varNames = self.__dymola.readTrajectoryNames(self.resultFileName + ".mat")
		varNames.sort()
		return varNames
		
	
	def close(self):
		if self.__dymola is not None:
			self.__dymola.close()
			self.__dymola = None 